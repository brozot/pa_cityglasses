package com.master.cityglasses;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.master.cityglasses.ScheduleView.ScheduleEvent;
import com.master.cityglassessmartphone.domain.Place;

public class PlaceView extends View {

	// Const Animation
	private static final int ANIMATION_STEP = 3;
	private static final int ANIMATION_DURATION = 100;
	private static final int ANIMATION_MESSAGE = 84;
	private static final int ANIMATION_REFRESH = 30;
	private static final int ANIMATION_REFRESH_PAUSE = 1000;

	// Const Position
	private static final int PADDING = 8; // in dp
	private static final int SIZE_NAME_BACKGROUND = 30;

	// Const TextSite
	private static final int TEXT_SIZE_NAME = 8;

	// background bitmap
	private Bitmap bitmapRight;
	private Bitmap bitmapCenter;
	private Bitmap bitmapLeft;
	private Bitmap bitmapIn;
	private Paint bitmapPaint;
	private Bitmap bitmapNoPicture;

	// Name block
	private String nameRight = "nameR";
	private String nameCenter = "nameC";
	private String nameLeft = "nameL";
	private String nameIn = "nameIN";
	private Rect nameBackgroundRect;
	private TextPaint nameTextPaint;
	private Paint nameBackgroundPaint;
	private LinearGradient nameGradient;
	private int textNameYPosition;

	// Distance block
	private String distanceRight = "disR";
	private String distanceCenter = "disC";
	private String distanceLeft = "distL";
	private String distanceIn = "distIN";
	private Rect distanceBackgroundRect;
	private TextPaint distanceTextPaint;
	private Paint distanceBackgroundPaint;
	private Paint selectorPaint;
	private Rect selectorRect;

	// Animation
	private int currentIncrement = 0;
	private boolean isAnimated = false;
	private boolean stopAnimation = true;
	private boolean isRightEntry = false;
	private boolean isLeftEntry = false;
	private int index;
	
	private boolean isInizialized = false;
	
	private PlaceEvent listener;

	// Content
	private List<Place> places;

	// position
	private int padding = 0;
	int blockWidht = 0;
	int blockHeight = 0;

	public interface PlaceEvent{
		public void listMoved(int index);
	}
	
	private Handler animHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			
			
			
			Log.i("animation","andler"+new Date(System.currentTimeMillis()).getSeconds());
			if (index <= 0 && isLeftEntry) {
				Log.i("update place","update places "+index);
				updatePlaces();
				invalidate();
			} else if (index >= places.size()-1 && isRightEntry) {
				Log.i("update place","update places 2 "+index);
				updatePlaces();
				invalidate();
			}else if(index >0 && index <places.size()-1){
				Log.i("update donf","update donf "+index);
				updatePlaces();
				invalidate();
			}else{
				isAnimated = false;
				animHandler.removeMessages(ANIMATION_MESSAGE);
			}
			
			currentIncrement += ANIMATION_STEP;
		}
	};

	public PlaceView(Context context) {
		super(context);
		Log.i("construct ", "construct 1");
		init();
	}

	public PlaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Log.i("construct ", "construct 2");
		init();
	}

	public PlaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		Log.i("construct ", "construct 3");
		init();
	}

	// initialisation appel� par le constructeur.
	private void init() {

		Log.i("init","init"+index);
		// background bitmap
		bitmapRight = null;
		bitmapCenter = null;
		bitmapLeft = null;
		bitmapIn = null;
		bitmapPaint = new Paint();
		
		selectorPaint = new Paint();
		selectorPaint.setStyle(Paint.Style.STROKE);
		selectorPaint.setStrokeWidth(4);
		selectorPaint.setColor(getContext().getResources().getColor(R.color.blue));
		selectorRect = new Rect();
		
		bitmapNoPicture = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.nopicture);
		// name block
		nameBackgroundRect = new Rect();
		nameBackgroundPaint = new Paint();
		nameTextPaint = new TextPaint();

		nameGradient = new LinearGradient(0, 0, 0, 80, Color.BLACK,
				Color.TRANSPARENT, Shader.TileMode.MIRROR);
		nameBackgroundPaint.setShader(nameGradient);
		nameTextPaint.setColor(Color.WHITE);
		textNameYPosition = (int) ((nameTextPaint.descent() - nameTextPaint.ascent()) / 2);
		// dp to px
		padding = dpToPx(PADDING);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		

		blockWidht = (int) ((float) canvas.getWidth() / 3);
		Log.i("this is the real blockWidth","real bliockwidth : "+blockWidht);
		blockHeight = canvas.getHeight();
		
		if(!isInizialized){
			selectorRect.set(blockWidht, 4, 2*blockWidht-4, blockHeight);
		}

		
		nameRight = (String) TextUtils.ellipsize(nameRight, nameTextPaint,
				blockWidht, TruncateAt.END);
		
		nameCenter = (String) TextUtils.ellipsize(nameCenter,
				nameTextPaint, blockWidht, TruncateAt.END);
		
		nameLeft = (String) TextUtils.ellipsize(nameLeft, nameTextPaint,
				blockWidht, TruncateAt.END);
		
		nameIn = (String) TextUtils.ellipsize(nameIn, nameTextPaint,
				blockWidht, TruncateAt.END);
		
		
		int movement = 0; 
		
		if(isRightEntry){
			movement = -computeTranslation(ANIMATION_DURATION, currentIncrement, blockWidht);
		}else{
			movement = computeTranslation(ANIMATION_DURATION, currentIncrement, blockWidht);
		}
		
		Log.i("compute ","compute "+movement);

		if (bitmapRight != null) {
			canvas.drawBitmap(bitmapRight, 2 * blockWidht+movement, 0, bitmapPaint);
		}

		if (bitmapCenter != null) {
			canvas.drawBitmap(bitmapCenter, blockWidht+movement, 0, bitmapPaint);
		}

		if (bitmapLeft != null) {
			canvas.drawBitmap(bitmapLeft, 0+movement, 0, bitmapPaint);
		}

		int bitmapInPos = 0;

		if (isRightEntry) {
			bitmapInPos = 3 * blockWidht+movement;
			nameBackgroundRect.set(0, 0, blockWidht * 4, 80);

		} else {
			bitmapInPos = -blockWidht+movement;
			nameBackgroundRect.set(-blockWidht, 0, blockWidht * 3, 80);
		}

		if (bitmapIn != null) {
			canvas.drawBitmap(bitmapIn, bitmapInPos, 0, bitmapPaint);
		}

		canvas.drawRect(nameBackgroundRect, nameBackgroundPaint);
		canvas.drawText(nameRight, blockWidht * 2 + padding+movement, 3*padding
				+ textNameYPosition, nameTextPaint);
		canvas.drawText(nameCenter, blockWidht + padding+movement, 3*padding
				+ textNameYPosition, nameTextPaint);
		canvas.drawText(nameLeft, 0 + padding+movement, 3*padding + textNameYPosition,
				nameTextPaint);
		canvas.drawText(nameIn, bitmapInPos + padding ,3* padding
				+ textNameYPosition, nameTextPaint);
		
		canvas.drawRect(selectorRect, selectorPaint);

		if (currentIncrement > ANIMATION_DURATION && !stopAnimation) {
			currentIncrement = 0;
			animHandler.removeMessages(ANIMATION_MESSAGE);
			animHandler.sendEmptyMessageDelayed(ANIMATION_MESSAGE,
					ANIMATION_REFRESH_PAUSE);
			if (isRightEntry) {
				index--;
			} else {
				index++;
			}
			if(listener != null){
				listener.listMoved(index);
			}
			
		} else if (currentIncrement > ANIMATION_DURATION && stopAnimation) {
			currentIncrement = 0;
			if (isRightEntry) {
				index--;
			} else {
				index++;
			}
			isAnimated = false;
			stopAnimation = false;
			isRightEntry = false;
			isLeftEntry = false;
			animHandler.removeMessages(ANIMATION_MESSAGE);
			if(listener != null){
				listener.listMoved(index);
			}
		} else if(isAnimated) {
			animHandler.removeMessages(ANIMATION_MESSAGE);
			animHandler.sendEmptyMessageDelayed(ANIMATION_MESSAGE,
					ANIMATION_REFRESH);
		}
		
		if(!isInizialized){
			isInizialized = true;
			updatePlaces();
			invalidate();
		}
	}

	public void setPlaces(List<Place> places) {
		this.places = new ArrayList<Place>(places);
	}

	private void updatePlaces() {
		
		if(places == null){
			return;
		}
		
		//Log.i("try update","try update "+currentIncrement);

	
		if(currentIncrement == 0){
			Log.i("index","update index "+index);
			try{
				bitmapNoPicture = Bitmap.createScaledBitmap(bitmapNoPicture, blockWidht, blockHeight, true);
			}catch (Exception e){
				
			}

			
			//Right
			try {
				Place placeRight = places.get(index - 1);
				bitmapRight = placeRight.loadInMemory(getContext());
				int[] dim = bitmapCrop(bitmapRight);
				Log.i("dimension : ","dimension right : "+dim[0]+" _ "+dim[1]);
				if(dim[0]!=0 && dim[1]!=0 && bitmapRight!=null){
					bitmapRight = Bitmap.createBitmap(bitmapRight, 0,0,dim[0],dim[1]);
					if(bitmapRight.getWidth()<blockWidht){
						bitmapRight = Bitmap.createScaledBitmap(bitmapRight, blockWidht, blockHeight, true);
					}
				}else{
					bitmapRight = bitmapNoPicture;
				}
				nameRight = placeRight.getName();
				distanceRight = placeRight.getId();
			} catch (Exception e) {
				e.printStackTrace();
				Log.i("title","error right ");
				nameRight = "";
				distanceRight = "";
				if (bitmapRight != null) {
					bitmapRight = null;
				}

			}
			
			//center
			try {
				Place placeCenter = places.get(index);
				bitmapCenter = placeCenter.loadInMemory(getContext());
				int[] dim = bitmapCrop(bitmapCenter);
				Log.i("dimension : ","dimension center : "+dim[0]+" _ "+dim[1]);
				if(dim[0]!=0 && dim[1]!=0 && bitmapCenter!=null){
					bitmapCenter = Bitmap.createBitmap(bitmapCenter, 0,0,dim[0],dim[1]);
					if(bitmapCenter.getWidth()<blockWidht){
						bitmapCenter = Bitmap.createScaledBitmap(bitmapCenter, blockWidht, blockHeight, true);
					}
				}else{
					bitmapCenter = bitmapNoPicture;
				}
				nameCenter = placeCenter.getName();
				distanceCenter = placeCenter.getId();
				Log.i("namecenter","namecenter"+nameCenter+" "+blockWidht);

				Log.i("namecenter","namecenter"+nameCenter);
				Log.i("bitmap", "bitmpa center " + bitmapCenter);
			} catch (Exception e) {
				e.printStackTrace();
				Log.i("title","error center ");
				nameCenter = "";
				distanceCenter ="";
				if (bitmapCenter != null) {
					bitmapCenter = null;
				}
				Log.i("bitmap", "bitmpa center error");

			}
			
			//left
			try {
				Place placeLeft = places.get(index + 1);
				bitmapLeft = placeLeft.loadInMemory(getContext());		
				int[] dim = bitmapCrop(bitmapLeft);
				Log.i("dimension : ","dimension left : "+dim[0]+" _ "+dim[1]);
				if(dim[0]!=0 && dim[1]!=0 && bitmapLeft!=null){
					bitmapLeft = Bitmap.createBitmap(bitmapLeft, 0,0,dim[0],dim[1]);
					if(bitmapLeft.getWidth()<blockWidht){
						bitmapLeft = Bitmap.createScaledBitmap(bitmapLeft, blockWidht, blockHeight, true);
					}
				}else{
					bitmapLeft = bitmapNoPicture;
				}
				nameLeft = placeLeft.getName();
				distanceLeft = placeLeft.getId();
			} catch (Exception e) {
				e.printStackTrace();
				Log.i("title","error left ");
				nameLeft = "";
				distanceLeft = "";
				if (bitmapLeft != null) {
					bitmapLeft = null;
				}

			}

			//IN
			if (isRightEntry) {
				try {
					Place placeIn = places.get(index - 2);
					bitmapIn = placeIn.loadInMemory(getContext());
					int[] dim = bitmapCrop(bitmapIn);
					if(dim[0]!=0 && dim[1]!=0 && bitmapIn!=null){
						bitmapIn = Bitmap.createBitmap(bitmapIn, 0,0,dim[0],dim[1]);
						if(bitmapIn.getWidth()<blockWidht){
							bitmapIn = Bitmap.createScaledBitmap(bitmapIn, blockWidht, blockHeight, true);
						}
					}else{
						bitmapIn = bitmapNoPicture;
					}
					nameIn = placeIn.getName();
					distanceIn = placeIn.getId();
				} catch (Exception e) {
					Log.i("title","error inRight ");
					e.printStackTrace();
					nameIn = "";
					distanceIn = "";
					if (bitmapIn != null) {
						bitmapIn = null;
					}

				}
			} else {
				try {
					Place placeIn = places.get(index + 2);
					bitmapIn = placeIn.loadInMemory(getContext());
					int[] dim = bitmapCrop(bitmapIn);
					if(dim[0]!=0 && dim[1]!=0 && bitmapIn!=null){
						bitmapIn = Bitmap.createBitmap(bitmapIn, 0,0,dim[0],dim[1]);
						if(bitmapIn.getWidth()<blockWidht){
							bitmapIn = Bitmap.createScaledBitmap(bitmapIn, blockWidht, blockHeight, true);
						}
					}else{
						bitmapIn = bitmapNoPicture;
					}
					nameIn = placeIn.getName();
					distanceIn = placeIn.getId();
				} catch (Exception e) {
					Log.i("title","error inleft ");
					e.printStackTrace();
					nameIn = "";
					distanceIn = "";
					if (bitmapIn != null) {
						bitmapIn = null;
					}

				}
			}
			
			Log.i("title","title right "+nameRight);
			Log.i("title","title Left "+nameLeft);
			Log.i("title","title center "+nameCenter);
			Log.i("title","title in "+nameIn);
			
		}
		

	}
	
	private int[] bitmapCrop(Bitmap bitmap){
		
		int[] dim = {0,0};
		
		if(bitmap != null){
			Log.i("bitmap","bitmap crop "+bitmap.getWidth()+"  :  "+blockWidht);
			if(bitmap.getWidth()>blockWidht) {
				dim[0] = blockWidht;
			}else{
				dim[0] = bitmap.getWidth()-1;
			}
			if(bitmap.getHeight()>blockHeight){
				dim[1] = blockHeight;
			}else{
				Log.i("bitmap height","bitmap height "+bitmap.getHeight());
				dim[1] = bitmap.getHeight()-1;
			}
			
		}
		return dim;
	}

	private int dpToPx(int dp) {
		DisplayMetrics displayMetrics = getContext().getResources()
				.getDisplayMetrics();
		int px = Math.round(dp
				* (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	public void goToLeft() {

		if (!isAnimated && !isLeftEntry) {
			Log.i("view","view go to left");
			isLeftEntry = true;
			isRightEntry = false;
			isAnimated = true;
			stopAnimation = false;
			currentIncrement = 0;
			animHandler.sendEmptyMessage(ANIMATION_MESSAGE);
		}
		

	}

	public void goToRight() {

		if (!isAnimated && !isRightEntry) {
			Log.i("view","view go to left");
			isRightEntry = true;
			isLeftEntry = false;
			isAnimated = true;
			stopAnimation = false;
			currentIncrement = 0;
			Log.i("animation right","animation right"+places);
			animHandler.sendEmptyMessage(ANIMATION_MESSAGE);
		}
		
	}
	
	public void stopMovement(){
		if (isAnimated) {
			stopAnimation = true;
		}
		
	}
	
	public static int computeTranslation(int durationAnimation, int currentIncrement, int movement){
		float increment = (float)movement/(float)durationAnimation;
		return (int) (increment*(float)currentIncrement);
	}
	
	public Place getCurrentPlace(){
		Place place = null;
		if(places != null){
			place = places.get(index);
		}
		return place;
	}
	
    public void setListener(PlaceEvent listener){
    	this.listener = listener;
    }
    
    public int getPlaceIndex(){
    	return index;
    }

	public void setIndex(int indexPlace) {
		this.index = indexPlace;
		currentIncrement = 0;
		updatePlaces();
		invalidate();
	}

	public void update() {
		currentIncrement = 0;
		updatePlaces();
    	invalidate();
		
	}
	
	public void destroy() {
    	animHandler.removeMessages(ANIMATION_MESSAGE);
    	isAnimated = false;
    	isRightEntry = false;
    	isLeftEntry = false;
    	stopAnimation = true;
    }

}
