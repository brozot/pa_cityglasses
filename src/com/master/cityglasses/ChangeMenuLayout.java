package com.master.cityglasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

public class ChangeMenuLayout extends TextView {

	protected static final int RED = 0;
	protected static final int GREEN = 120;
	
	public static final int TYPE_LEFT = 0;
	public static final int TYPE_RIGHT = 1;
	public static final int TYPE_UP = 2;
	
	protected int dx;
	protected int width;
	
	protected Paint backgroudPaint;
	protected Paint textPaint;
	protected Rect rect;
	
	protected int type = 0;

	public ChangeMenuLayout(Context context) {
		super(context);
		init();
	}

	public ChangeMenuLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public ChangeMenuLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		setWillNotDraw(false);
		backgroudPaint = new Paint();
		backgroudPaint.setColor(Color.HSVToColor(toGradient(0)));
		textPaint = getPaint();
		textPaint.setTextAlign(Align.CENTER);
		textPaint.setColor(Color.WHITE);
		setType(type);
		rect = new Rect();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		
		rect.set(0, 0, canvas.getWidth(), canvas.getHeight());
		int x = canvas.getWidth()/2;
		int y = canvas.getHeight()/2;
		int yText = (int) (y - (textPaint.ascent()/2+textPaint.descent()/2));
		
		switch (type) {
		case 0:
			canvas.translate(dx-width, 0);
			canvas.drawRect(rect, backgroudPaint);
			canvas.rotate(-90, x, y);
			canvas.drawText((String) this.getText(), x, yText, textPaint);
			canvas.rotate(90, x, y);
			break;
		case 1:
			Log.i("paint dx","paint dx :"+dx);
			canvas.rotate(180, x, y);
			canvas.translate(dx-width, 0);
			canvas.drawRect(rect, backgroudPaint);
			canvas.rotate(-90, x, y);
			canvas.drawText((String) this.getText(), x, yText, textPaint);
			canvas.rotate(90, x, y);
			canvas.rotate(180, x, y);
			break;
		case 2:
			canvas.translate(0, dx-width);
			canvas.drawRect(rect, backgroudPaint);
			canvas.drawText((String) this.getText(), x, yText, textPaint);
			break;
		default:
			canvas.drawText("error", x, yText, textPaint);
			break;
		}
		
	}
	
	public void setDx(int percent) {
		percent = dxRules(percent);
		dx = dxToPixel(percent, width);
		Log.i("print dx","print dx "+dx+" percent "+percent+" width "+width);
		backgroudPaint.setColor(Color.HSVToColor(toGradient(percent)));
		this.invalidate();
	}
	
	
	//Normlise la taille de la vue entre 1 et 100
	private static int dxRules(int percent){
		if(percent > 100){
			percent = 100;
		}else if(percent<0){
			percent = 0;
		}
		return percent;
	}
	
	//d�fini le d�placement en pixel de la transition
	private static int dxToPixel(int percent, int width){
		float div = (float)width/100;
		return (int)(div*percent);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if(type == TYPE_UP){
			width = getHeight();
		}else{
			width = getWidth();
		}
		dx = 0;
	}
	
	private static float[] toGradient(int percent){
		float[] tab = {0,100,100};
		final int gradient = Math.abs(GREEN-RED);
		final float div = (float)gradient/100;
		tab[0] = div*percent;
		return tab;
	}
	
	public void setType(int type){
		if(type > 2){
			this.type = 2;
		}else{
			this.type = type;
		}
		
		if(type == 2){
			width = getHeight();
		}
		
	}
	
	public int getType(){
		return type;
	}

}
