package com.master.cityglasses;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.master.cityglasses.PlaceView.PlaceEvent;
import com.master.cityglassessmartphone.domain.Place;

public class NewPlaceFragment extends Fragment implements PlaceEvent{
	
	private PlaceView placeView;
	private ProgressPoint progressPointView;
	
	private boolean isToRight = false;
	private boolean isToLeft = false;
	private boolean isBlocked = false;
	
	private int index = 0;
	
	private List<Place> places;
	
	/**
	 * Get a new instance of PlaceFratment
	 * @return a new instance of PlaceFragment
	 */
	public static NewPlaceFragment newInstance(){
		NewPlaceFragment fragment = new NewPlaceFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	public NewPlaceFragment() {
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.new_fragment_place, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Log.i("initi 2","initi 2");
		placeView = (PlaceView) view.findViewById(R.id.fragment_place_placeview);
		progressPointView = (ProgressPoint) view.findViewById(R.id.fragment_place_progresspoint);
		progressPointView.setCheckedColor(R.color.white);
		
		if(places != null){
			placeView.setPlaces(places);
			progressPointView.setNumber(places.size());
			progressPointView.setBackgroundResource(R.color.blue);
			progressPointView.setChecked(index+1);
			placeView.setIndex(index);
			placeView.update();
		}
		
		placeView.setListener(this);
		
	}
	
	/**
	 * Set places used by placeView
	 * @param places List of Place
	 */
	public void setPlaces(List<Place> places){
		this.places = new ArrayList<Place>(places);
		if(placeView != null && progressPointView != null){
			placeView.setPlaces(this.places);
			progressPointView.setNumber(places.size());
			placeView.update();
		}
	}
	
	public void goToRight(){
		if(isToLeft && !isBlocked){
			Log.i("gos to right","stop");
			placeView.stopMovement();
			isToLeft = false;
		}else if(!isToLeft && !isBlocked){
			Log.i("gos to right","start");
			placeView.goToRight();
			isToRight = true;
			progressPointView.setBackgroundResource(R.color.green);
		}
	}
	
	public void goToLeft(){
		if(isToRight && !isBlocked){
			Log.i("gos to left","stop");
			isToRight = false;
			placeView.stopMovement();
		}else if(!isToRight && !isBlocked){
			Log.i("gos to left","starts");
			isToLeft = true;
			progressPointView.setBackgroundResource(R.color.green);
			placeView.goToLeft();
		}
	}
	
	public void stopMovement(){
		if(isToLeft || isToRight){
			isToLeft = false;
			isToRight = false;
			progressPointView.setBackgroundResource(R.color.blue);
			placeView.stopMovement();
		}
	}
	
	public void blockAndUnblock(){
		
		if(!isBlocked){
			placeView.stopMovement();
			isBlocked = true;
			progressPointView.setBackgroundColor(Color.RED);
		}else{
			isBlocked = false;
			progressPointView.setBackgroundColor(Color.GREEN);
		}
		
	}
	
	public Place getCurrentPlace(){
		return placeView.getCurrentPlace();
	}

	@Override
	public void listMoved(int index) {
		progressPointView.setChecked(index+1);
		
	}
	
	public int getPlaceIndex(){
		return placeView.getPlaceIndex();
	}

	public void setPlaceIndex(int indexPlace) {
		Log.i("setPlaceIndex","setPlaceIndex "+indexPlace);
		this.index = indexPlace;
		placeView.setIndex(indexPlace);
		progressPointView.setChecked(indexPlace+1);
	}
	
	@Override
	public void onDestroy() {
		Log.i("on destroy","destroy fragment ");
		placeView.destroy();
		super.onDestroy();
	}


}
