package com.master.cityglasses;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.master.cityglassessmartphone.domain.Schedule;

/**
 * Created by nicolas on 10.05.14.
 */
public class ScheduleView extends View {

    private static final int TEXT_TITLE_SIZE = 30;
    private static final int BLOCK_PADDING = 8;

    private static final int ANIM_MESSAGE = 99;

    private static final int ANIMATION_STEP = 30;
    private static final int ANIMATION_STEP_PAUSE = 2500;
    private static final int STEP = 3;

    private int durationAnimation = 100;
    private int currentIncrement = 0;
    
    private int index = 0;

    private Rect canvasBackgroundRect;
    private Rect canvasCenterBackgroundRect;

    private Paint canvasBackgroundPaint;
    private Paint canvasCenterBackgroundPaint;
    private Paint textTitlePaint;

    private TextPaint textFromPaint;
    private TextPaint textToPaint;

    private int blockRight = 0;

    private int textTitleHeight = 0;
    private int lineTitleWidth = 0;
    private int lineInWidth = 0;
    private int lineLeftWidth = 0;
    private int lineRightWidth = 0;
    private int lineCenterWidth = 0;
    private int hourInWidth = 0;
    private int hourLeftWidth = 0;
    private int hourRightWidth = 0;
    private int hourCenterWidth = 0;

    private String lineTitle1 = "";
    private String lineTitle2 = "";
    private String lineTitle3 = "";
    private String lineTitleEntry = "";
    private String toTitle = "";
    private String fromTitle = "";

    private String currentFrom = "current from";
    private String nextFrom = "next from";

    private String currentTo = "current to";
    private String nextTo = "next to";
    private StringBuilder textSpeak; 

    private int blockPadding;
    private int hourYPosition;

    private String lineIn = "";
    private String lineRight = "";
    private String lineCenter = "";
    private String lineLeft = "";
    private String hourIn = "";
    private String hourRight = "";
    private String hourCenter = "";
    private String hourLeft = "";
    
    private List<Schedule> schedules = new ArrayList<Schedule>(); 

    private boolean isRunningAnimation = false;
    private boolean stopAnimation = false;
    private boolean isRightEntryAnimation = false;
    private boolean isLeftEntryAnimation = false;
    private boolean isInitialized = false;
    
    private ScheduleEvent listener;
    
    private TextToSpeech tts;
    
    //Animation listener
    public interface ScheduleEvent{
    	public void listMoved(int index);
    }

    private Handler animHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            
            if (index <= 0 && isLeftEntryAnimation) {
            	updateFields();
				invalidate();
			} else if (index >= schedules.size()-1 && isRightEntryAnimation) {
				updateFields();
				invalidate();
			}else if(index >0 && index <schedules.size()-1){
				updateFields();
				invalidate();
			}else{
				isRunningAnimation = false;
			}
            
            currentIncrement += STEP;
        }
    };

    public ScheduleView(Context context) {
        super(context);
        init();
    }

    public ScheduleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ScheduleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init(){

        this.blockPadding = dpToPx(BLOCK_PADDING);
        
        this.textSpeak = new StringBuilder();

        this.lineTitle1 = "ligne";
        this.lineTitle2 = "ligne";
        this.lineTitle3 = "ligne";
        this.toTitle = "à";
        this.fromTitle = "de";

        this.lineIn = "00000";
        this.lineRight = "111111";
        this.lineCenter = "22222";
        this.lineLeft = "33333";

        this.hourIn = "00:00";
        this.hourRight = "11:11";
        this.hourCenter = "22:22";
        this.hourLeft = "33:33";

        canvasBackgroundRect = new Rect();
        canvasCenterBackgroundRect = new Rect();

        canvasBackgroundPaint = new Paint();
        canvasCenterBackgroundPaint = new Paint();
        canvasBackgroundPaint.setColor(Color.GRAY);
        canvasCenterBackgroundPaint.setColor(Color.BLUE);

        textTitlePaint = new Paint();
        textTitlePaint.setColor(Color.WHITE);
        textTitlePaint.setTextSize(dpToPx(TEXT_TITLE_SIZE));
        textTitlePaint.setStyle(Paint.Style.FILL);

        textFromPaint = new TextPaint();
        textFromPaint.setColor(Color.WHITE);
        textFromPaint.setTextSize(dpToPx(TEXT_TITLE_SIZE));
        textFromPaint.setStyle(Paint.Style.FILL);

        textToPaint = new TextPaint();
        textToPaint.setColor(Color.WHITE);
        textToPaint.setTextSize(dpToPx(TEXT_TITLE_SIZE));
        textToPaint.setStyle(Paint.Style.FILL);

        lineTitleWidth = (int)textTitlePaint.measureText(lineTitle1, 0, lineTitle1.length());

    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        int canvasWidth = canvas.getWidth();
        int canvasHeight = canvas.getHeight();

        int smallBlockWidth = (canvasWidth/2)/2;
        int bigBlockWidth = (canvasWidth/2);

        int blockLeft = 0;
        int blockCenter = smallBlockWidth;
        blockRight = smallBlockWidth + bigBlockWidth;

        lineInWidth = (int)textTitlePaint.measureText(lineIn, 0, lineIn.length());
        lineLeftWidth = (int)textTitlePaint.measureText(lineLeft, 0, lineLeft.length());
        lineCenterWidth = (int)textTitlePaint.measureText(lineCenter, 0, lineCenter.length());
        lineRightWidth = (int)textTitlePaint.measureText(lineRight, 0, lineRight.length());


        hourInWidth = (int)textTitlePaint.measureText(hourIn, 0, hourIn.length());
        hourLeftWidth = (int)textTitlePaint.measureText(hourLeft, 0, hourLeft.length());
        hourCenterWidth = (int)textTitlePaint.measureText(hourCenter, 0, hourCenter.length());
        hourRightWidth = (int)textTitlePaint.measureText(hourRight, 0, hourRight.length());

        textTitleHeight = (int) (textTitlePaint.descent() - textTitlePaint.ascent());

        hourYPosition = canvas.getHeight() / 3;

        canvasBackgroundRect.set(0, 0, canvasWidth, canvasHeight);
        canvasCenterBackgroundRect.set(blockCenter, 0, blockCenter + bigBlockWidth, canvasHeight);

        canvas.drawRect(canvasBackgroundRect, canvasBackgroundPaint);
        canvas.drawRect(canvasCenterBackgroundRect, canvasCenterBackgroundPaint);

        canvas.drawText(fromTitle, blockCenter + blockPadding, canvasHeight - 3 * textTitleHeight - 2 * blockPadding, textTitlePaint);
        canvas.drawText(toTitle, blockCenter + blockPadding, canvasHeight - textTitleHeight - blockPadding, textTitlePaint);

        textFromPaint.setAlpha(alphaValue(durationAnimation, currentIncrement));
        textToPaint.setAlpha(alphaValue(durationAnimation, currentIncrement));
        
        if(currentIncrement <= durationAnimation/2){
        	
        	currentFrom = (String) TextUtils.ellipsize(currentFrom, textFromPaint, bigBlockWidth, TextUtils.TruncateAt.END);
        	nextFrom = (String) TextUtils.ellipsize(nextFrom, textFromPaint, bigBlockWidth, TextUtils.TruncateAt.END);
        	
        	currentTo = (String) TextUtils.ellipsize(currentTo, textToPaint, bigBlockWidth, TextUtils.TruncateAt.END);
        	nextTo = (String) TextUtils.ellipsize(nextTo, textToPaint, bigBlockWidth, TextUtils.TruncateAt.END);
        	
            canvas.drawText(currentFrom, blockCenter + blockPadding, canvasHeight - 2*textTitleHeight - 2*blockPadding, textToPaint);
            canvas.drawText(currentTo, blockCenter + blockPadding, canvasHeight - blockPadding, textFromPaint);

        }else{
            canvas.drawText(nextFrom, blockCenter + blockPadding, canvasHeight - 2*textTitleHeight - 2*blockPadding, textToPaint);
            canvas.drawText(nextTo, blockCenter + blockPadding, canvasHeight - blockPadding, textFromPaint);

        }


        if(isRightEntryAnimation){
            //In ------------------------------------------------------
            canvas.drawText(lineTitleEntry, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth, blockRight+smallBlockWidth/2-lineTitleWidth/2),
                    canvasHeight/2, textTitlePaint );
            canvas.drawText(lineIn, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth, blockRight+smallBlockWidth/2 - lineInWidth/2),
                    canvasHeight/2+textTitleHeight, textTitlePaint );
            //hour
            canvas.drawText(hourIn, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth, blockRight+smallBlockWidth/2 - hourInWidth/2),
                    blockPadding+textTitleHeight, textTitlePaint );
            //------------------------------------------------------

            //Right------------------------------------------------------
            canvas.drawText(lineTitle1, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth/2-lineTitleWidth/2, blockCenter+blockPadding),
                    linePosition(durationAnimation, currentIncrement, canvasHeight/2, blockPadding+textTitleHeight), textTitlePaint );
            canvas.drawText(lineRight, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth/2-lineRightWidth/2, blockCenter+blockPadding),
                    linePosition(durationAnimation, currentIncrement, canvasHeight/2+textTitleHeight, blockPadding+textTitleHeight+textTitleHeight), textTitlePaint );
            //hour
            canvas.drawText(hourRight, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth/2 - hourRightWidth/2, blockCenter+bigBlockWidth/2 - hourRightWidth/2),
                    linePosition(durationAnimation, currentIncrement, blockPadding+textTitleHeight, hourYPosition), textTitlePaint );
            //------------------------------------------------------

            //Center------------------------------------------------------
            canvas.drawText(lineTitle2, linePosition(durationAnimation, currentIncrement, blockCenter+blockPadding, blockLeft+smallBlockWidth/2 - lineTitleWidth/2),
                    linePosition(durationAnimation, currentIncrement, blockPadding+textTitleHeight, canvasHeight/2), textTitlePaint );
            canvas.drawText(lineCenter, linePosition(durationAnimation, currentIncrement, blockCenter+blockPadding, blockLeft+smallBlockWidth/2 - lineCenterWidth/2),
                    linePosition(durationAnimation, currentIncrement, blockPadding+textTitleHeight+textTitleHeight, canvasHeight/2+textTitleHeight), textTitlePaint );
            //hour
            canvas.drawText(hourCenter, linePosition(durationAnimation, currentIncrement, blockCenter+bigBlockWidth/2 - hourCenterWidth/2, smallBlockWidth/2 - hourCenterWidth/2),
                    linePosition(durationAnimation, currentIncrement, hourYPosition, blockPadding+textTitleHeight), textTitlePaint );
            //------------------------------------------------------

            //Left------------------------------------------------------
            canvas.drawText(lineTitle3, linePosition(durationAnimation, currentIncrement, blockLeft+smallBlockWidth/2 - lineTitleWidth/2, blockLeft - lineTitleWidth),
                    canvasHeight/2, textTitlePaint );
            canvas.drawText(lineLeft, linePosition(durationAnimation, currentIncrement, blockLeft+smallBlockWidth/2 - lineRightWidth/2, blockLeft - lineRightWidth),
                    canvasHeight/2+textTitleHeight, textTitlePaint );
            //hour
            canvas.drawText(hourLeft, linePosition(durationAnimation, currentIncrement, smallBlockWidth/2 - hourCenterWidth/2, blockLeft- hourCenterWidth),
                    blockPadding+textTitleHeight, textTitlePaint );
            //------------------------------------------------------
        }

        if(isLeftEntryAnimation || !isInitialized){
        	isInitialized = true;
            //In ------------------------------------------------------
            canvas.drawText(lineTitleEntry, linePosition(durationAnimation, currentIncrement, blockLeft-lineTitleWidth, blockLeft+smallBlockWidth/2-lineTitleWidth/2),
                    canvasHeight/2, textTitlePaint );
            canvas.drawText(lineIn, linePosition(durationAnimation, currentIncrement, blockLeft-lineInWidth, blockLeft+smallBlockWidth/2-lineInWidth/2),
                    canvasHeight/2+textTitleHeight, textTitlePaint );
            //hour
            canvas.drawText(hourIn, linePosition(durationAnimation, currentIncrement, blockLeft-hourInWidth, blockLeft+smallBlockWidth/2 - hourInWidth/2),
                    blockPadding+textTitleHeight, textTitlePaint );
            //------------------------------------------------------

            //Right------------------------------------------------------
            canvas.drawText(lineTitle1, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth/2-lineTitleWidth/2, blockRight+smallBlockWidth),
                    linePosition(durationAnimation, currentIncrement, canvasHeight/2, canvasHeight/2), textTitlePaint );
            canvas.drawText(lineRight, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth/2-lineRightWidth/2, blockRight+smallBlockWidth),
                    canvasHeight/2+textTitleHeight, textTitlePaint );
            //hour
            canvas.drawText(hourRight, linePosition(durationAnimation, currentIncrement, blockRight+smallBlockWidth/2 - hourRightWidth/2, blockRight+smallBlockWidth),
                    blockPadding+textTitleHeight, textTitlePaint );
            //------------------------------------------------------

            //Center------------------------------------------------------
            canvas.drawText(lineTitle2, linePosition(durationAnimation, currentIncrement, blockCenter+blockPadding, blockRight+smallBlockWidth/2 - lineTitleWidth/2),
                    linePosition(durationAnimation, currentIncrement, blockPadding+textTitleHeight, canvasHeight/2), textTitlePaint );
            canvas.drawText(lineCenter, linePosition(durationAnimation, currentIncrement, blockCenter+blockPadding, blockRight+smallBlockWidth/2 - lineCenterWidth/2),
                    linePosition(durationAnimation, currentIncrement, blockPadding+textTitleHeight+textTitleHeight, canvasHeight/2+textTitleHeight), textTitlePaint );
            //hour
            canvas.drawText(hourCenter, linePosition(durationAnimation, currentIncrement, blockCenter+bigBlockWidth/2 - hourCenterWidth/2, blockRight+ smallBlockWidth/2 - hourCenterWidth/2),
                    linePosition(durationAnimation, currentIncrement, hourYPosition, blockPadding+textTitleHeight), textTitlePaint );
            //------------------------------------------------------

            //Left------------------------------------------------------
            canvas.drawText(lineTitle3, linePosition(durationAnimation, currentIncrement, blockLeft+smallBlockWidth/2 - lineTitleWidth/2, blockCenter+blockPadding),
                    linePosition(durationAnimation, currentIncrement, canvasHeight/2, blockPadding+textTitleHeight), textTitlePaint );
            canvas.drawText(lineLeft, linePosition(durationAnimation, currentIncrement, blockLeft+smallBlockWidth/2 - lineRightWidth/2, blockCenter+blockPadding),
                    linePosition(durationAnimation, currentIncrement, canvasHeight/2+textTitleHeight, blockPadding+2*textTitleHeight), textTitlePaint );
            //hour
            canvas.drawText(hourLeft, linePosition(durationAnimation, currentIncrement, smallBlockWidth/2 - hourCenterWidth/2, blockCenter+bigBlockWidth/2 - hourRightWidth/2),
                    linePosition(durationAnimation, currentIncrement, blockPadding+textTitleHeight, hourYPosition), textTitlePaint );
            //------------------------------------------------------
        }




        if(currentIncrement>=durationAnimation && !stopAnimation){
            currentIncrement = 0;
            if(isRightEntryAnimation){
            	Log.i("decrement","decrement" +index);
            	index --;
            }else if(isLeftEntryAnimation){
            	Log.i("increments","increment "+index);
            	index ++;
            }
            animHandler.removeMessages(ANIM_MESSAGE);
            animHandler.sendEmptyMessageDelayed(ANIM_MESSAGE, ANIMATION_STEP_PAUSE);
            if(listener != null){
            	listener.listMoved(index);
            }
            if(tts != null){
				//tts.speak(textSpeak.toString(), TextToSpeech.QUEUE_FLUSH, null);
			}
        }else if(currentIncrement>=durationAnimation && stopAnimation){
            if(isRightEntryAnimation){
            	Log.i("decrement","decrement" +index);
            	index --;
            }else if(isLeftEntryAnimation){
            	Log.i("increments","increment "+index);
            	index ++;
            }
            if(listener != null){
            	listener.listMoved(index);
            }
        	isRunningAnimation = false;
            isRightEntryAnimation = false;
            isLeftEntryAnimation = false;
            animHandler.removeMessages(ANIM_MESSAGE);
            if(tts != null){
				//tts.speak(textSpeak.toString(), TextToSpeech.QUEUE_FLUSH, null);
			}
            currentIncrement = 0;
        }else if(currentIncrement != 0) {
            animHandler.removeMessages(ANIM_MESSAGE);
            animHandler.sendEmptyMessageDelayed(ANIM_MESSAGE, ANIMATION_STEP);
        }
        
        Log.i("invalidate","invalidate");



    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    //Calcule le alpha en fonction de l'animation
    private static int alphaValue (int durationAnimation, int currentIncrement){

        int halfAnimation = durationAnimation/2;
        float increment = (255f / (float)halfAnimation);

        int alpha = 0;

        if(currentIncrement <= halfAnimation){
            alpha = (int)(255-currentIncrement*increment);
        }else{
            alpha = (int)((currentIncrement-halfAnimation)*increment);
        }
        
        if(alpha>255){
        	alpha = 255;
        }
        return alpha;
    }

    private static int linePosition(int durationAnimation, int currentIncrement, int from, int to){

        int distance = to-from;
        float increment = (float)distance/(float)durationAnimation;
        int position = (int)(increment*currentIncrement);
        position = from + position;

        return position;
    }

    private static String getTime(){
        long timeInMillis = System.currentTimeMillis();
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(timeInMillis);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        StringBuilder builder = new StringBuilder();

        //Ajoute un zéro si plus petit que 10
        if(hour < 10){
            builder.append("0");
            builder.append(hour);
        }else{
            builder.append(hour);
        }
        builder.append(":");
        if(minute<10){
            builder.append("0");
            builder.append(minute);
        }else{
            builder.append(minute);
        }
        String time = builder.toString();

        return time;
    }

    public void startEntryRight(){

    	if (!isRunningAnimation) {
    		Log.i("je start a droite","je gauche");
			isRightEntryAnimation = true;
			isLeftEntryAnimation = false;
			isRunningAnimation = true;
			stopAnimation = false;
			currentIncrement = 0;
			animHandler.sendEmptyMessage(ANIM_MESSAGE);
		}
    }

    public void startEntryLeft(){
    	if (!isRunningAnimation) {
			isLeftEntryAnimation = true;
			isRightEntryAnimation = false;
			isRunningAnimation = true;
			stopAnimation = false;
			currentIncrement = 0;
			animHandler.sendEmptyMessage(ANIM_MESSAGE);
		}
		
    }

    public void stopMovement(){
        if(isRunningAnimation){
            stopAnimation = true;
        }
    }
    
    public void setSchedulesList(List<Schedule> schedules){
    	Log.i("SchedulView","setSchedule : "+schedules.size());
    	this.schedules = new ArrayList<Schedule>(schedules);
    }
    
    private void updateFields(){
    	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    	Log.i("update","update "+currentIncrement);
    	textSpeak.setLength(0);
        
        if(currentIncrement == 0 && !schedules.isEmpty()){
        	try{
        		Schedule scheduleRight = schedules.get(index-1);
        		lineRight = scheduleRight.getLigneNumber();
        		hourRight = sdf.format(scheduleRight.getDeparture());
        		lineTitle1 = "ligne";
        		if(isRightEntryAnimation){
        			nextFrom = scheduleRight.getName();
        			nextTo = scheduleRight.getTo();
        			if(!hourRight.isEmpty()){
                    	textSpeak.append(hourRight.substring(0, 2));
                    	textSpeak.append(" heure ");
                    	textSpeak.append(hourRight.substring(3, 5));
                    	textSpeak.append(" ");
                    	textSpeak.append(nextTo);
                    }
        		}
        	}catch (Exception e) {
				lineRight = "";
				hourRight = "";
				nextFrom = "";
    			nextTo = "";
    			lineTitle1 = "";
			}
        	
        	try{
        		Schedule scheduleCenter = schedules.get(index);
        		lineCenter = scheduleCenter.getLigneNumber();
        		hourCenter = sdf.format(scheduleCenter.getDeparture());
        		currentFrom = scheduleCenter.getName();
    			currentTo = scheduleCenter.getTo();
    			lineTitle2 = "ligne";
        	}catch (Exception e) {
				lineCenter = "";
				hourCenter = "";
				lineTitle2 = "";
				if(isRightEntryAnimation){
					currentFrom = "";
					currentTo = "";
				}
				
			}
        	
        	try{
        		Schedule scheduleLeft = schedules.get(index+1);
        		lineLeft = scheduleLeft.getLigneNumber();
        		hourLeft = sdf.format(scheduleLeft.getDeparture());
        		lineTitle3 = "ligne";
        		if(isLeftEntryAnimation){
        			nextFrom = scheduleLeft.getName();
        			nextTo = scheduleLeft.getTo();
        			if(!hourLeft.isEmpty()){
                    	textSpeak.append(hourLeft.substring(0, 2));
                    	textSpeak.append(" heure ");
                    	textSpeak.append(hourLeft.substring(3, 5));
                    	textSpeak.append(" ");
                    	textSpeak.append(nextTo);
                    }
        		}
        	}catch (Exception e) {
				lineLeft = "";
				hourLeft = "";
    			lineTitle3 = "";
				if(isLeftEntryAnimation){
					nextFrom = "";
	    			nextTo = "";
				}
				
			}
        	
        	if(isRightEntryAnimation){
        		try{
            		Schedule scheduleIn= schedules.get(index-2);
            		lineIn = scheduleIn.getLigneNumber();
            		hourIn = sdf.format(scheduleIn.getDeparture());
            		lineTitleEntry = "ligne";
            	}catch (Exception e) {
					lineIn = "";
					hourIn = "";
					lineTitleEntry = "";
				}
        	}
        	
        	if(isLeftEntryAnimation){
        		try{
            		Schedule scheduleIn= schedules.get(index+2);
            		lineIn = scheduleIn.getLigneNumber();
            		hourIn = sdf.format(scheduleIn.getDeparture());
            		lineTitleEntry = "ligne";
            	}catch (Exception e) {
					lineIn = "";
					hourIn = "";
					lineTitleEntry = "";
				}
        	}
        	
        	Log.i("to string speak","to stroing speak "+textSpeak.toString());
            
            if(tts != null){
				tts.speak(textSpeak.toString(), TextToSpeech.QUEUE_FLUSH, null);
			}
        	
        }
    }
    public void setListener(ScheduleEvent listener){
    	this.listener = listener;
    }
    
    public void setTts(TextToSpeech tts){
    	this.tts = tts;
    }
    
    public void update(){
    	isInitialized = false;
    	updateFields();
    	invalidate();
    }
    
    public void destroy(){
    	animHandler.removeMessages(ANIM_MESSAGE);
    }
    

}
