package com.master.cityglasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class Movement extends View{
	
	private Paint linePaint;
	private Paint linePositionPaint;
	private Paint rectPaint;
	private Rect rect;
	
	private int width;
	private int height;
	
	private int verticalPosition = 0;
	private int horizontalPosition = 0;
	
	private boolean isInitialized = false;

	public Movement(Context context) {
		super(context);
		init();
	}
	
	public Movement(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public Movement(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init(){
		
		//met la ligne au centre du carr�
		if(!isInitialized){
			verticalPosition = height/2;
			horizontalPosition = height/2;
			isInitialized = true;
		}
		
		linePaint = new Paint();
		linePaint.setColor(Color.BLACK);
		linePaint.setStyle(Paint.Style.STROKE);
		linePaint.setStrokeWidth(2);
		
		rectPaint = new Paint();
		rectPaint.setColor(Color.BLUE);
		rectPaint.setStyle(Paint.Style.STROKE);
		rectPaint.setStrokeWidth(2);
		
		linePositionPaint = new Paint();
		linePositionPaint.setColor(Color.RED);
		linePositionPaint.setStyle(Paint.Style.STROKE);
		linePositionPaint.setStrokeWidth(8);
		rect = new Rect();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		width = canvas.getWidth();
		height = canvas.getHeight();
		
		canvas.drawLine(0, height/2, width, height/2, linePaint);
		canvas.drawLine(width/2, 0, width/2, height, linePaint);
		
		rect.set(0 ,0 ,canvas.getWidth(), canvas.getHeight());
		canvas.drawRect(rect, rectPaint);
		
		canvas.drawLine(width/2, height/2, horizontalPosition, height/2, linePositionPaint);
		canvas.drawLine(width/2, height/2, width/2, verticalPosition, linePositionPaint);
		
	}
	
	public void setAxeVertical(float value, float max){
		this.verticalPosition = -position(height/2, max, value)+height/2;
		invalidate();
	}
	
	public void setAxeHorizontal(float value, float max){
		this.horizontalPosition = -position(width/2, max, value)+width/2;
		invalidate();
	}
	
	private static int position(int real, float max, float value){		
		return (int) (((float)real / max)*value);
	}

}
