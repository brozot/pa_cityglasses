package com.master.cityglasses;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.storage.StorageManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.master.cityglasses.ScheduleView.ScheduleEvent;
import com.master.cityglassessmartphone.domain.Schedule;

public class NewScheduleFragment extends Fragment implements ScheduleEvent{
	
	private ScheduleView scheduleView;
	private ProgressPoint progressPointView;
	private List<Schedule> schedules;
	
	private boolean isToRight = false;
	private boolean isToLeft = false;
	
	private boolean isBlocked = false;
	
	private TextToSpeech tts;
	
	public static NewScheduleFragment newInstance(){
		NewScheduleFragment fragment = new NewScheduleFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_schedule, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		scheduleView = (ScheduleView) view.findViewById(R.id.fragment_schedule_scheduleview);
		progressPointView = (ProgressPoint) view.findViewById(R.id.fragment_schdule_progresspoint);
		
		progressPointView.setCheckedColor(R.color.white);
		progressPointView.setBackgroundResource(R.color.blue);
		
		if(!schedules.isEmpty()){
			Log.i("schedule fragment ","schedule onView : "+schedules.size());
			scheduleView.setSchedulesList(schedules);
			progressPointView.setNumber(schedules.size());
			progressPointView.setChecked(1);
			scheduleView.update();
		}
		scheduleView.setListener(this);
		scheduleView.setTts(tts);
		
	}
	
	public void setSchedules(List<Schedule> schedules){
		Log.i("schedule fragment fucking ","schedule fucking set : "+schedules.size());
		this.schedules = new ArrayList<Schedule>(schedules);
		if(scheduleView != null && progressPointView != null){
			Log.i("schedule fragment ","schedule set : "+schedules.size());
			scheduleView.setSchedulesList(this.schedules);
			scheduleView.update();
		}
		
	}
	
	public void goToRight(){
		Log.i("gos to right","go to right");
		if(isBlocked){
			return;
		}
		if(isToLeft){
			Log.i("gos to right","stop");
			scheduleView.stopMovement();
			isToLeft = false;
		}else{
			Log.i("gos to right","start");
			progressPointView.setBackgroundResource(R.color.green);
			scheduleView.startEntryRight();
			isToRight = true;
		}
	}
	
	public void goToLeft(){
		if(isBlocked){
			return;
		}
		Log.i("go to left","go to left");
		if(isToRight){
			Log.i("gos to left","stop");
			scheduleView.stopMovement();
			isToRight = false;
		}else{
			Log.i("gos to left","starts");
			progressPointView.setBackgroundResource(R.color.green);
			scheduleView.startEntryLeft();
			isToLeft = true;
		}
	}
	
	public void stopMovement(){
		if(isToLeft || isToRight){
			progressPointView.setBackgroundResource(R.color.blue);
			isToLeft = false;
			isToRight = false;
			scheduleView.stopMovement();
		}
	}
	
	public void blockAndUnblock(){
		if(isBlocked){
			isBlocked = false;
			progressPointView.setBackgroundResource(R.color.blue);
		}else{
			isBlocked = true;
			progressPointView.setBackgroundResource(R.color.red);
			scheduleView.stopMovement();
		}
	}

	
	//***************************************
	//
	// Listener
	//
	//***************************************
	
	@Override
	public void listMoved(int index) {
		progressPointView.setChecked(index+1);
	}
	
	public void setTts(TextToSpeech tts){
		this.tts = tts;
		if(scheduleView != null){
			scheduleView.setTts(tts);
		}
	}
	
	@Override
	public void onDestroy() {
		scheduleView.destroy();
		super.onDestroy();
	}
}

