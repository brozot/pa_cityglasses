package com.master.cityglasses;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.master.cityglassessmartphone.domain.Place;
import com.master.cityglassessmartphone.domain.Request;
import com.master.cityglassessmartphone.domain.Schedule;
import com.master.cityglassessmartphone.domain.Station;

public class BluetoothManager {

	private static final String TAG_LOG = "CityGlasses - BluetoothManager";

	private static final String DEVICE_NAME = "M100";
	private static final UUID BLUETOOTH_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805f9b34fb");

	private static BluetoothManager myInstance = null;
	private static BluetoothAdapter adapter;

	private List<BluetoothMessageListener> listeners = new ArrayList<BluetoothMessageListener>();

	private static BluetoothSocket socket;

	private static Context context;

	private InputStream inputStream;
	private OutputStream outputStream;
	private ObjectInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;

	// Liste des horaires renvoy�s par le t�l�phone
	private List<Schedule> schedules = new ArrayList<Schedule>();
	private List<Place> places = new ArrayList<Place>();

	// Permet de passer les message du thread d'attente de message au thread UI
	private Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			// regarde le type de message pour le diriger vers le listener
			// ad�quiat
			if (!listeners.isEmpty()) {
				switch (msg.what) {
				case 1: // message de type Image (Map)
					for (BluetoothMessageListener listener : listeners) {
						listener.receiveMap((Bitmap) msg.obj);
					}
					break;

				case 2: // Message de type horaire
					List<Schedule> list = (List<Schedule>) (msg.obj);
					for (BluetoothMessageListener listener : listeners) {
						Log.i("messagesss", "message schedule" + schedules.size());
						listener.reveiveSchedule(schedules);
					}
					schedules.clear();
					break;
					
				case 3: // Message de type horaire
					for (BluetoothMessageListener listener : listeners) {
						Log.i("message", "message places" + places.size());
						listener.reveivePlace(places);
					}
					places.clear();
					break;
				case 4: // le device est connect�
					for(BluetoothMessageListener listener : listeners){
						listener.isConnected();
					}
					
				case 5: // restart l'attente de connection
					for(BluetoothMessageListener listener : listeners){
						initBluetooth();
					}

				default:
					Log.w(TAG_LOG, "Handler : unknow type of message");
					break;
				}
			}
		}
	};

	/**
	 * Create or get a instance of bluetoothManager
	 * 
	 * @param context Application context
	 * @return a Instance of BluetoothManager
	 */
	public static BluetoothManager getInstance(Context context) {
		if (myInstance == null) {
			myInstance = new BluetoothManager(context);
		}
		return myInstance;
	}

	private BluetoothManager(Context context) {
		BluetoothManager.context = context;
		BluetoothManager.adapter = BluetoothAdapter.getDefaultAdapter();
		initBluetooth();
	}

	public void initBluetooth() {

		adapter.setName(DEVICE_NAME);
		schedules.clear();

		// active le bluetooth si il est d�sactiv�
		if (!isBluetoothEnable()) {
			bluetoothEnable();
		}

		// TODO regarder pour rendre le device reconnaissable
		if (adapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			//TODO rendre le device reconnaissable
		}

		startServerSoket();

	}

	public void startServerSoket() {
		String name = "lunette M100";
		Log.i("accept", "attente accept init");
		//BluetoothManager.adapter = BluetoothAdapter.getDefaultAdapter();
		try {
			Log.i("isDecovering","is decovering : "+adapter.isDiscovering());
			BluetoothManager.adapter = BluetoothAdapter.getDefaultAdapter();
			final BluetoothServerSocket server = adapter
					.listenUsingRfcommWithServiceRecord(name, BLUETOOTH_UUID);
			Thread acceptThread = new Thread(new Runnable() {

				@Override
				public void run() {
					Log.i("accept", "attente accept");
					try {
						Log.i("enter in thread","enter in thdread");
						BluetoothSocket socketTemps = server.accept();
						server.close();
						Log.i("enter in thread","accept?");
						socket = socketTemps;
						outputStream = socket.getOutputStream();
						inputStream = socket.getInputStream();
						objectOutputStream = new ObjectOutputStream(
								outputStream);
						objectInputStream = new ObjectInputStream(inputStream);
					} catch (IOException e) {
						Log.i("error connection server socket", "server accept");
					}

					// averti l'application qu'elle � �t� connect� � un
					// t�l�phone
					Message msg = handler.obtainMessage();
					msg.what = 4;
					msg.obj = null;
					msg.arg1 = 1;

					handler.sendMessage(msg);

					Log.i("accept", "accept");
					listenMessage();

				}
			});
			acceptThread.start();
		} catch (IOException e) {
			Log.i("error connection server socket", "startServer socket");
			cancel();
			e.printStackTrace();
			
		}
		
		
	}

	private void listenMessage() {

		Station station = null;

		while (true) {
			try {
				Object object = objectInputStream.readObject();
				if (object instanceof Station) {
					Log.i("casting", "is a instance");
					station = (Station) object;
				}

				if (station != null) {
					Log.i("stations", "stations : " + station.toString());
				}

				if (object instanceof byte[]) {
					Log.i("byte", "byte : ");
					Bitmap bitmap = getBitmap((byte[]) object);

					Message msg = handler.obtainMessage();
					msg.what = 1;
					msg.obj = bitmap;
					msg.arg1 = 1;

					handler.sendMessage(msg);

				}

				if (object instanceof Schedule) {
					Schedule schedule = (Schedule) object;

					if (schedule.getId() != null) {
						schedules.add(schedule);
					} else {
						Message msg = handler.obtainMessage();
						msg.what = 2;
						msg.obj = schedules;
						msg.arg1 = 2;

						handler.sendMessage(msg);
					}
				}
				
				if(object instanceof Place){
					Place place = (Place) object;
					Log.i("places","places getId : "+place.getId());
					if(place.getId() != null){
						Log.i("placees","places : "+place.getBitmap());
						place.saveInMemory(context);
						places.add(place);
					}else{
						Message msg = handler.obtainMessage();
						msg.what = 3;
						msg.obj = places;
						msg.arg1 = 3;

						handler.sendMessage(msg);
					}
				}
			} catch (Exception e) {
				Log.i("error connection server socket",
						"listen exception socket");
				e.printStackTrace();
				resetConnection();
				break;
			}
		}
	}

	public Bitmap getBitmap(byte[] bitmap) {
		return BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length);
	}

	public void sendBluetoothObject(Request request) {
		try {
			if (objectOutputStream != null) {
				objectOutputStream.writeObject(request);
				objectOutputStream.reset();
				// outputStream.write(message.getBytes());
			}
		} catch (IOException e) {
			Log.i("error connection server socket", "error send");
			resetConnection();
			e.printStackTrace();
		}

	}

	public void resetConnection() {
		try {
			if (inputStream != null) {
				inputStream.close();
			}
			if (outputStream != null) {
				outputStream.close();
			}
			if (objectInputStream != null) {
				objectInputStream.close();
			}
			if (objectOutputStream != null) {
				objectOutputStream.close();
			}
			if (socket != null) {
				socket.close();
			}
			
			Message msg = handler.obtainMessage();
			msg.what = 5;
			msg.obj = null;
			msg.arg1 = 5;

			handler.sendMessage(msg);

		} catch (IOException e) {
			cancel();
			e.printStackTrace();
		}

	}

	public void cancel() {
		try {
			if (inputStream != null) {
				inputStream.close();
			}
			if (outputStream != null) {
				outputStream.close();
			}
			if (objectInputStream != null) {
				objectInputStream.close();
			}
			if (objectOutputStream != null) {
				objectOutputStream.close();
			}
			if (socket != null) {
				socket.close();
			}
			
			

			myInstance = null;

			Log.i("connection", "close");
		} catch (IOException e) {
			Log.i("error connection server socket", "error close");
		}
	}

	public boolean bluetoothEnable() {
		return adapter.enable();
	}

	public boolean bluetoothDisable() {
		return adapter.disable();
	}

	// setter and getter
	public boolean isInstantiated() {
		boolean isInstantiated = false;

		if (myInstance != null) {
			isInstantiated = true;
		}

		return isInstantiated;
	}

	public boolean isBluetoothEnable() {
		return adapter.isEnabled();
	}

	public void setListener(BluetoothMessageListener listener) {
		listeners.add(listener);
	}
}
