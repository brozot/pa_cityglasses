package com.master.cityglasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

public class InfoMenuView extends TextView{
	
	public static final int MAX_ALPHA = 200;
	protected static final int LINE_SIZE = 3;
	protected int alpha;
	
	public static final int TYPE_LEFT = 0;
	public static final int TYPE_RIGHT = 1;
	public static final int TYPE_UP = 2;
	
	protected Paint backgroudPaint;
	protected Paint textPaint;
	protected Rect rect;
	
	protected Paint line;
	protected Rect lineRect;
	protected LinearGradient linearGradientRight;
	protected LinearGradient linearGradientLeft;
	protected LinearGradient linearGradientTop;

	protected int type = 1;
	
	protected int number = 0;

	public InfoMenuView(Context context) {
		super(context);
		init();
	}
	
	public InfoMenuView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public InfoMenuView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init(){
		setWillNotDraw(false);
		Log.i("r�init","R�init");
		alpha = 255;
		backgroudPaint = new Paint();
		backgroudPaint.setColor(getContext().getResources().getColor(R.color.white));
		textPaint = getPaint();
		textPaint.setTextAlign(Align.CENTER);
		textPaint.setColor(Color.BLACK);
		line = new Paint();
		lineRect = new Rect();
		linearGradientLeft = new LinearGradient(0, 0, 3, 0, Color.TRANSPARENT, Color.BLACK, Shader.TileMode.MIRROR);
		linearGradientRight = new LinearGradient(3, 0, 0, 0, Color.TRANSPARENT, Color.BLACK, Shader.TileMode.MIRROR);
		linearGradientTop = new LinearGradient(0, 0, 0, 3, Color.TRANSPARENT, Color.BLACK, Shader.TileMode.MIRROR);
		setType(type);
		rect = new Rect();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		
		backgroudPaint.setAlpha(100);
		
		rect.set(0, 0, canvas.getWidth(), canvas.getHeight());
		int x = canvas.getWidth()/2;
		int y = canvas.getHeight()/2;
		int yText = (int) (y - textPaint.getTextSize()/2-(textPaint.ascent()/2+textPaint.descent()/2));
		int yTextNumber = (int) (y +textPaint.getTextSize()/2-(textPaint.ascent()/2+textPaint.descent()/2));
		
		canvas.saveLayerAlpha(0, 0, canvas.getWidth(), canvas.getHeight(), alpha, Canvas.HAS_ALPHA_LAYER_SAVE_FLAG);
		canvas.drawRect(rect, backgroudPaint);
		
		
		switch (type) {
		case 0:
			line.setShader(linearGradientLeft);
			lineRect.set(canvas.getWidth()-LINE_SIZE, 0, canvas.getWidth(), canvas.getHeight());
			canvas.drawRect(lineRect, line);
			canvas.rotate(-90, x, y);
			canvas.drawText((String) this.getText(), x, yText, textPaint);
			canvas.drawText(String.valueOf(number), x, yTextNumber, textPaint);
			canvas.rotate(90, x, y);
			break;
			
		case 1:
			line.setShader(linearGradientRight);
			lineRect.set(0, 0, LINE_SIZE, canvas.getHeight());
			canvas.drawRect(lineRect, line);
			canvas.rotate(90, x, y);
			canvas.drawText((String) this.getText(), x, yText, textPaint);
			canvas.drawText(String.valueOf(number), x, yTextNumber, textPaint);
			canvas.rotate(-90, x, y);
			break;
			
		case 2:
			line.setShader(linearGradientTop);
			lineRect.set(0, canvas.getHeight()-LINE_SIZE, canvas.getWidth(), canvas.getHeight());
			canvas.drawRect(lineRect, line);
			canvas.drawText((String) this.getText(), x, yText, textPaint);
			canvas.drawText(String.valueOf(number), x, yTextNumber, textPaint);
			break;

		default:
			canvas.drawText((String) this.getText(), x, yText, textPaint);
			canvas.drawText(String.valueOf(number), x, yTextNumber, textPaint);
			break;
		}
	}
	
	public void setMyAlpha(int percent){
		percent = alphaRules(percent);
		this.alpha = toAlpha(percent);
		this.invalidate();
	}
	
	private static int alphaRules(int percent){
		if(percent > 100){
			percent = 100;
		}else if(percent<0){
			percent = 0;
		}
		return percent;
	}
	
	//d�fini le d�placement en pixel de la transition
	private static int toAlpha(int percent){
		float div = (float)MAX_ALPHA/100;
		percent = (int)(div*percent);
		return InfoMenuView.MAX_ALPHA - percent;
	}
	
	public void setType(int type){
		if(type > 2){
			this.type = 2;
		}else{
			this.type = type;
		}
		
	}
	
	public int getType(){
		return type;
	}
	
	public void setNumber(int number){
		this.number = number;
	}
	
	public int getNumber(){
		return number;
	}




}
