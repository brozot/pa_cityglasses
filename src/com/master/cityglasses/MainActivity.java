package com.master.cityglasses;

import java.security.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.master.cityglasses.api.action.ActionUser;
import com.master.cityglasses.api.action.NewActionUser;
import com.master.cityglasses.api.action.NewActionUserListener;
import com.master.cityglasses.api.action.NewHeadMovement;
import com.master.cityglasses.api.action.OnActionUserListener;
import com.master.cityglassessmartphone.domain.Place;
import com.master.cityglassessmartphone.domain.Request;
import com.master.cityglassessmartphone.domain.Schedule;

public class MainActivity extends Activity implements BluetoothMessageListener,
		OnActionUserListener, OnInitListener, NewActionUserListener, LocationListener {

	private MainFragment mainFragment;
	private NewScheduleFragment scheduleFragment;
	private NewPlaceFragment placeFragment;
	private PlaceDetailFragment placeDetailFragment;
	private CaptureFragment captureFragment;
	
	private static final int MIN_DISTANCE = 50;

	private TextView connectionRequest;
	
	private ActionUser action;
	private BluetoothManager bluetooth;
	
	private Movement movement;

	private List<Schedule> schedule = new ArrayList<Schedule>();
	private List<Place> places = new ArrayList<Place>();

	private Context context;
	
	private boolean isToUp = false;

	private TextToSpeech tts;
	
	private NewActionUser actionUser;
	
	private PowerManager.WakeLock wakeLock;
	
	private LocationManager locationManager;
	
	private int indexPlace = 0;
	
	private boolean isConnected = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);
		
		//Ecran toujours allum�
		final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.screenBrightness = 1F;
        getWindow().setAttributes(layoutParams);

		this.context = this;

		// Instantiation des fragments
		mainFragment = MainFragment.newInstance();
		scheduleFragment = NewScheduleFragment.newInstance();
		placeFragment = NewPlaceFragment.newInstance();
		placeDetailFragment = PlaceDetailFragment.newInstance();
		captureFragment = CaptureFragment.newInstance();
		
		movement = (Movement) findViewById(R.id.activity_main_movement);
		connectionRequest = (TextView) findViewById(R.id.activity_main_connection_request);

		// mise en place du fragment main
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.add(R.id.activity_main_container, mainFragment,
				MainFragment.TAG_FRAGMENT).commit();

		bluetooth = BluetoothManager.getInstance(context);
		bluetooth.setListener(this);

		//action = ActionUser.getInstance(context);
		//action.setOnActionUserListener(this);
		
		new NewHeadMovement(this);
		
		actionUser = new NewActionUser(context);
		actionUser.addActionUserListener(this);

		tts = new TextToSpeech(this, this);
		
		
	    
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
	    criteria.setAccuracy(Criteria.ACCURACY_FINE);
	    criteria.setAltitudeRequired(false);//true if required
	    criteria.setBearingRequired(false);//true if required
	    criteria.setCostAllowed(true);
	    criteria.setPowerRequirement(Criteria.POWER_LOW);
	    String provider = locationManager.getBestProvider(criteria, true);
	    Log.i("provider","provider : "+provider);
		locationManager.requestLocationUpdates(provider, 10000, MIN_DISTANCE, this);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// notifie action d'un �v�nement bouton
		//action.setKeyEvent(event);
		actionUser.resetOrigin();
		actionUser.setEventButton(event);
		// g�re le zoom de la map (MainFragment)
		if (mainFragment.isVisible()) {
			if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
				mainFragment.decrementMapZoom();
			} else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
				mainFragment.incrementMapZoom();
			}
		}

		return true;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {

		if(keyCode == KeyEvent.KEYCODE_ENTER && captureFragment.isVisible()){
			captureFragment.takeAPicture();
		}
		
		if (placeDetailFragment.isVisible() && keyCode == KeyEvent.KEYCODE_ENTER){
			bluetooth.sendBluetoothObject(new Request(Request.TYPE_REQUEST_NAVIGATION, "google.navigation:q=" +47.055855+","+6.742835+"&mode=w"));
		}
		
		// Si le bouton retour est appuy�, fermeture de l'applications
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			if (mainFragment.isVisible()) {
				finish();
			} else if (scheduleFragment.isVisible()) {
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.setCustomAnimations(R.anim.fragment_slide_in_right,
						R.anim.fragment_slide_out_left);
				ft.replace(R.id.activity_main_container, mainFragment).commit();
				Log.i("schedule list","schedule list : "+schedule.size());
				mainFragment.setIsScheduleInfoVisible(false);
				if(!schedule.isEmpty()){
					mainFragment.setVisibleSchedule(schedule.size());
				}
			}else if (placeFragment.isVisible()) {
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.setCustomAnimations(R.anim.fragment_slide_in_left,
						R.anim.fragment_slide_out_right);
				ft.replace(R.id.activity_main_container, mainFragment).commit();
				if(!places.isEmpty()){
					mainFragment.setVisibleSchedule(places.size());
				}
			}else if(captureFragment.isVisible()){
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.setCustomAnimations(R.anim.fragment_slide_in_bottom,
						R.anim.fragment_slide_out_bottom);
				ft.replace(R.id.activity_main_container, mainFragment).commit();
			}
 

		}
		return true;
	}

	@Override
	protected void onResume() {
		this.wakeLock.acquire();
		super.onResume();

	}
	
	@Override
	protected void onPause() {
		wakeLock.release();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		Log.i("destroy", "destrroy");
		bluetooth.cancel();
		action.unregisterd();
		BitmapUtils.deleteAllBitmap(context);
		super.onDestroy();
	}
	
	// ////////////////////////////////////////////////////////////////////
	//
	// Location Listener
	//
	// ///////////////////////////////////////////////////////////////////
	
	
	@Override
	public void onLocationChanged(Location location) {
		Toast.makeText(context, "update location", Toast.LENGTH_SHORT).show();
		mainFragment.UpdateMap(location.getAltitude(), location.getLongitude());
		bluetooth.sendBluetoothObject(new Request(Request.TYPE_REQUEST_MONUMENT, ""));
		bluetooth.sendBluetoothObject(new Request(Request.TYPE_REQUEST_SCHEDULE, "http://transport.opendata.ch/v1/locations?x="+location.getLatitude()+"&y="+location.getLongitude()+""));
		
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Toast.makeText(context, ""+status, Toast.LENGTH_SHORT).show();
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	// ////////////////////////////////////////////////////////////////////
	//
	// Listener
	//
	// ///////////////////////////////////////////////////////////////////

	// -------------------------------------------------------------------
	// Bluetooth data Listerner
	// -------------------------------------------------------------------

	@Override
	public void receiveMap(Bitmap bitmap) {
		// TODO Auto-generated method stub

	}

	@Override
	public void reveiveSchedule(List<Schedule> schedules) {
		schedule = new ArrayList<Schedule>(schedules);

	}

	@Override
	public void reveivePlace(List<Place> places) {
		this.places = new ArrayList<Place>(places);
	}

	@Override
	public void isConnected() {
		isConnected = true;
		connectionRequest.setVisibility(View.GONE);
		

	}

	// -------------------------------------------------------------------
	// Movement listener
	// -------------------------------------------------------------------

	@Override
	public void goToRight() {
		Log.i("movement", "movement right");
		

	}

	@Override
	public void goToLeft() {
		
		Log.i("movement", "movement left");
		

	}

	@Override
	public void goToUp() {
		
		Log.i("movement", "movement up");
		//tts.speak("hautt", TextToSpeech.QUEUE_FLUSH, null);
		if (scheduleFragment.isVisible()) {
			scheduleFragment.blockAndUnblock();

		}
		if(placeFragment.isVisible()){
			isToUp = true;
			placeFragment.blockAndUnblock();
		}
		
		if(placeDetailFragment.isVisible()){
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.replace(R.id.activity_main_container, placeFragment).commit();
		}

	}

	@Override
	public void goToBottom() {
		
		Log.i("movement", "movement bottom");
		// TODO Auto-generated method stub
		//tts.speak("bas", TextToSpeech.QUEUE_FLUSH, null);
		
	}

	@Override
	public void goToRightWithAction() {
		
	}

	@Override
	public void goToLeftWithAction() {
		Log.i("main activity","try to send this fucking schedule");
		

	}

	@Override
	public void goToUpWithAction() {
		
	}

	@Override
	public void goToBottomWithAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateGoToRightWithAction(int value) {
		if (mainFragment.isVisible() && (!places.isEmpty())) {
			mainFragment.setRightValue(value);
		}
	}

	@Override
	public void updateGoToLeftWithAction(int value) {
		Log.i("update left", "update : " + value);
		if (mainFragment.isVisible() && (!schedule.isEmpty())) {
			mainFragment.setLeftValue(value);
		}

	}

	@Override
	public void updateGoToUpWithAction(int value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateGoToBottomWithAction(int value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			tts.setLanguage(Locale.FRENCH);
			tts.setSpeechRate(2.0f);
			scheduleFragment.setTts(tts);
		} else if (status == TextToSpeech.ERROR) {
			Toast.makeText(this, "error with text to speetch",
					Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void updateMovement(float[] angle, boolean isButton) {
		if (mainFragment.isVisible() && (!places.isEmpty()) && isButton) {
			if(angle[1]<0){
				mainFragment.setRightValue(valueInPercent(NewHeadMovement.LIMIT_RIGHT, angle[1]));
			}else{
				mainFragment.setLeftValue(valueInPercent(NewHeadMovement.LIMIT_LEFT, angle[1]));
			}
			
		}
		movement.setAxeHorizontal(angle[1], NewHeadMovement.LIMIT_LEFT);
		movement.setAxeVertical(angle[0], NewHeadMovement.LIMIT_TOP);
		
		if(mainFragment.isVisible() && isButton){
			if(angle[0]>0){
				int value = valueInPercent(NewHeadMovement.LIMIT_TOP, angle[0]);
				Log.i("up value","up value"+value+"  ,  "+angle[0]);
				mainFragment.setTopValue(value);
			}
		}
		
	}

	@Override
	public void movementRight(boolean isButton) {
		if(!isConnected){
			return;
		}
		
		Log.i("R movement","R movement "+isButton);
		if (mainFragment.isVisible() && (!places.isEmpty()) && isButton) {
			actionUser.setEventButton(null);
			placeFragment.setPlaces(places);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.anim.fragment_slide_in_right,
					R.anim.fragment_slide_out_left);
			ft.replace(R.id.activity_main_container, placeFragment).commit();
		}
		
		if (scheduleFragment.isVisible() && !isButton) {
			scheduleFragment.goToRight();

		}else if(placeFragment.isVisible() && ! isButton){
			placeFragment.goToRight();
		}
		
	}

	@Override
	public void movementLeft(boolean isButton) {
		if(!isConnected){
			return;
		}
		Log.i("L movement","L movement "+isButton);
		if (mainFragment.isVisible() && (!schedule.isEmpty()) && isButton) {
			actionUser.setEventButton(null);
			scheduleFragment.setSchedules(schedule);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.anim.fragment_slide_in_left,
					R.anim.fragment_slide_out_right);
			ft.replace(R.id.activity_main_container, scheduleFragment).commit();
		}
		
		if (scheduleFragment.isVisible() && !isButton) {
			scheduleFragment.goToLeft();
		}else if(placeFragment.isVisible() && !isButton){
			placeFragment.goToLeft();
		}
		
	}

	@Override
	public void movementTop(boolean isButton) {
		if(!isConnected){
			return;
		}
		Log.i("Top movement","Top movement "+isButton);
		
		if (scheduleFragment.isVisible() && !isButton) {
			scheduleFragment.blockAndUnblock();

		}
		if(placeFragment.isVisible() && !isButton){
			placeFragment.blockAndUnblock();
		}
		
		if(placeDetailFragment.isVisible() && !isButton){
			placeFragment.setPlaceIndex(indexPlace);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.anim.fragment_slide_in_top,
					R.anim.fragment_slide_out_bottom);
			ft.replace(R.id.activity_main_container, placeFragment).commit();
		}
		
		if(mainFragment.isVisible() && isButton){
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.anim.fragment_slide_in_top,
					R.anim.fragment_slide_out_bottom);
			ft.replace(R.id.activity_main_container, captureFragment).commit();
		}
		
	}

	@Override
	public void movementBottom(boolean isButton) {
		if(!isConnected){
			return;
		}
		Log.i("B movement","B movement "+isButton);
		
		if(placeFragment.isVisible() && !isButton){
			Place place = placeFragment.getCurrentPlace();
			indexPlace = placeFragment.getPlaceIndex();
			Log.i("index place ","index place : "+indexPlace );
			if(place != null && !isToUp){
				placeDetailFragment.setPlace(place);
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.setCustomAnimations(R.anim.fragment_slide_in_bottom, R.anim.fragment_slide_out_top);
				ft.replace(R.id.activity_main_container, placeDetailFragment).commit();
			}
		}
		
	}

	@Override
	public void movementCenterHorizontal(boolean isButton) {
		if(!isConnected){
			return;
		}
		Log.i("CH movement","CH movement "+isButton);
		if (scheduleFragment.isVisible() && !isButton) {
			scheduleFragment.stopMovement();
		}else if(placeFragment.isVisible() && !isButton){
			placeFragment.stopMovement();
		}
		
	}

	@Override
	public void movementCenterVertical(boolean isButton) {
		if(!isConnected){
			return;
		}
		Log.i("CV movement","CV movement "+isButton);
		
	}
	
	private static int valueInPercent(float max, float value){
		
		int values = Math.abs((int)(100/max * value));
		Log.i("calculate value","calculateValue "+values);
		return values;
	}


	
	
	

}
