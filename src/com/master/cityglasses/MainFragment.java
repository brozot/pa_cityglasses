package com.master.cityglasses;

import java.util.List;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.master.cityglassessmartphone.domain.Place;
import com.master.cityglassessmartphone.domain.Request;
import com.master.cityglassessmartphone.domain.Schedule;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class MainFragment extends Fragment implements BluetoothMessageListener{
	
	public static final String TAG_FRAGMENT = "main_fragment";
	
	private ChangeMenuLayout changeLeft;
	private InfoMenuView infoLeft;
	
	private ChangeMenuLayout changeRight;
	private InfoMenuView infoRight;
	
	private ChangeMenuLayout changeTop;
	private InfoMenuView infoTop;
	
	private SmoothProgressBar progress;
	private ImageView mapView;
	private ImageView centerView;
	boolean receive = false;
	
	private BluetoothManager manager;
	
	private int mapZoom = 14;
	
	private int numberSchedule = 0;
	private int numberPlace = 0;
	
	boolean isScheduleInfoVisible = false;
	boolean isPlaceInfoVisible = false;

	private double longitude = 6.9308205;
	private double latitude = 46.9923219;

	public static MainFragment newInstance(){
		MainFragment fragment = new MainFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	public MainFragment() {
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_main, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		changeLeft = (ChangeMenuLayout) view.findViewById(R.id.fragment_main_changemenu_left);
		infoLeft = (InfoMenuView) view.findViewById(R.id.fragment_main_infomenu_left);
		
		changeRight = (ChangeMenuLayout) view.findViewById(R.id.fragment_main_changemenu_right);
		infoRight = (InfoMenuView) view.findViewById(R.id.fragment_main_infomenu_right);
		
		changeTop = (ChangeMenuLayout) view.findViewById(R.id.fragment_main_changemenu_top);
		infoTop = (InfoMenuView) view.findViewById(R.id.fragment_main_infomenu_top);
		
		mapView = (ImageView) view.findViewById(R.id.fragment_main_picture_map);
		centerView = (ImageView) view.findViewById(R.id.fragment_main_picture_map_center);
		progress = (SmoothProgressBar)view.findViewById(R.id.fragment_main_progress);
		
		changeLeft.setType(ChangeMenuLayout.TYPE_LEFT);
		infoLeft.setType(InfoMenuView.TYPE_LEFT);
		
		changeRight.setType(ChangeMenuLayout.TYPE_RIGHT);
		infoRight.setType(InfoMenuView.TYPE_RIGHT);
		
		changeTop.setType(ChangeMenuLayout.TYPE_UP);
		infoTop.setType(InfoMenuView.TYPE_UP);
		
		//initialize
		changeLeft.setDx(0);
		changeRight.setDx(0);
		changeTop.setDx(100);
		
		manager = BluetoothManager.getInstance(getActivity());
		manager.setListener(this);
		
		infoTop.setVisibility(View.INVISIBLE);
		
		if(numberSchedule > 0){
			setVisibleSchedule(numberSchedule);
		}
		
		if(numberPlace > 0){
			setVisiblePlace(numberPlace);
		}
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		downloadNewMap(mapZoom);
	}
	

	//////////////////////////////////////////////////////////////////////
	//
	//	ChangeMenu
	//
	/////////////////////////////////////////////////////////////////////
	
	
	public void setLeftValue(int value){
		changeLeft.setDx(value);
		infoLeft.setMyAlpha(value);
	}
	
	public void setRightValue(int value){
		changeRight.setDx(value);
		infoRight.setMyAlpha(value);
	}
	
	public void setTopValue(int value){
		changeTop.setDx(value);
		//infoTop.setMyAlpha(value);
	}
	
	
	//////////////////////////////////////////////////////////////////////
	//
	//	InfoViewAnimation
	//
	/////////////////////////////////////////////////////////////////////
	
	/**
	 * Set the infoView for schedule visible with the number of schedule
	 * @param number number of schedule available
	 */
	public void setVisibleSchedule(int number){
		//Si la vue n'est pas visible on la rend visible avec une animation
		this.numberSchedule = number;
		infoLeft.setVisibility(View.VISIBLE);
		infoLeft.setNumber(number);
		infoLeft.setMyAlpha(0);
		if(!isScheduleInfoVisible){
			Log.i("schedule is visible","schedule is visible baby "+infoLeft.getWidth());
			Animation animation = new TranslateAnimation(Animation.ABSOLUTE, -(float)infoLeft.getWidth(), Animation.ABSOLUTE, 0.0f, Animation.ABSOLUTE, 0.0f, Animation.ABSOLUTE, 0.0f);
			startInfoAnimation(animation, infoLeft);
			isScheduleInfoVisible = true;
		}
		
	}
	
	/**
	 * Set the infoView for place visible with the number of place
	 * @param number number of schedule available
	 */
	public void setVisiblePlace(int number){
		//Si la vue n'est pas visible on la rend visible avec une animation
		this.numberPlace = number;
		infoRight.setVisibility(View.VISIBLE);
		infoRight.setNumber(number);
		infoRight.setMyAlpha(0);
		if(!isPlaceInfoVisible){
			Animation animation = new TranslateAnimation(Animation.ABSOLUTE, (float)infoLeft.getWidth(), Animation.ABSOLUTE, 0.0f, Animation.ABSOLUTE, 0.0f, Animation.ABSOLUTE, 0.0f);
			startInfoAnimation(animation, infoRight);
			isPlaceInfoVisible = true;
		}
		
	}
	
	
	/**
	 * Set the infoView for schedule invisible
	 * 
	 */
	public void setInvisibleSchedule() {
		//Si la vue est visible, on la rend invisible avec une animation
		if(isScheduleInfoVisible){
			Animation animation = new TranslateAnimation(Animation.ABSOLUTE, 0.0f, Animation.ABSOLUTE, -(float) infoLeft.getWidth(), Animation.ABSOLUTE, 0.0f, Animation.ABSOLUTE, 0.0f);
			startInfoAnimation(animation, infoLeft);
			isScheduleInfoVisible = false;
		}
		
	}
	
	/**
	 * Set the infoView for place invisible
	 * 
	 */
	public void setInvisiblePlace() {
		//Si la vue est visible, on la rend invisible avec une animation
		if(isPlaceInfoVisible){
			Animation animation = new TranslateAnimation(Animation.ABSOLUTE, 0.0f, Animation.ABSOLUTE, +(float) infoLeft.getWidth(), Animation.ABSOLUTE, 0.0f, Animation.ABSOLUTE, 0.0f);
			startInfoAnimation(animation, infoRight);
			isPlaceInfoVisible = false;
		}
		
	}
	
	//Ajoute un dur�e et un blocage � l'animation et d�marre l'animation pour la vue;
	private static void startInfoAnimation(Animation animation, InfoMenuView view){
		animation.setDuration(1500);
		animation.setFillAfter(true);
		view.startAnimation(animation);
	}
	
	//////////////////////////////////////////////////////////////////////
	//
	//	MapView
	//
	/////////////////////////////////////////////////////////////////////
	/**
	 * increment zoom level
	 */
	public void incrementMapZoom(){
		if(mapZoom < 21){
			mapZoom ++;
			downloadNewMap(mapZoom);
		}
	}
	/**
	 * decrement zoom level
	 */
	public void decrementMapZoom(){
		if(mapZoom > 0){
			mapZoom--;
			downloadNewMap(mapZoom);
		}
	}
	
	//Demande le t�l�chargement d'une nouvelle map
	private void downloadNewMap(int zoom){
		progress.setVisibility(View.VISIBLE);
		centerView.setVisibility(View.INVISIBLE);
		String url = "https://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitude+"&size=428x240&sensor=false&zoom="+zoom+"";
		Log.i("url","url "+url);
		manager.sendBluetoothObject(new Request(Request.TYPE_REQUEST_MAP, url));
	}
	
	//////////////////////////////////////////////////////////////////////
	//
	//	Listener
	//
	/////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------
	//Bluetooth data Listerner
	//-------------------------------------------------------------------

	//Si on recoit une image d'une nouvelle carte, on l'affiche
	@Override
	public void receiveMap(Bitmap bitmap) {
		progress.setVisibility(View.INVISIBLE);
		centerView.setVisibility(View.VISIBLE);
		mapView.setVisibility(View.VISIBLE);
		mapView.setImageBitmap(bitmap);	
	}

	//Quand des donn�es sur les arr�ts sont ramener, on montre la boite de dialogue 
	//pour afficher l'information a l'utilisateur
	@Override
	public void reveiveSchedule(List<Schedule> schedules) {
		if(schedules.isEmpty()){
			setInvisibleSchedule();
		}else{
			setVisibleSchedule(schedules.size());
		}
		
	}
	
	@Override
	public void reveivePlace(List<Place> places) {
		if(places.isEmpty()){
			setInvisiblePlace();
		}else{
			setVisiblePlace(places.size());
		}
		
	}

	@Override
	public void isConnected() {
	}
	
	public void setIsScheduleInfoVisible(boolean isVisible){
		this.isScheduleInfoVisible = isVisible;
	}
	
	public void setIsPlaceInfoVisible(boolean isVisible){
		this.isPlaceInfoVisible = isVisible;
	}
	
	public void UpdateMap(double latitude, double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
		downloadNewMap(mapZoom);
	}

	
	
	
	
	
	
	

}
