package com.master.cityglasses;

import java.util.List;

import android.graphics.Bitmap;

import com.master.cityglassessmartphone.domain.Place;
import com.master.cityglassessmartphone.domain.Schedule;

public interface BluetoothMessageListener {
	
	public void receiveMap(Bitmap bitmap);
	public void reveiveSchedule(List<Schedule> schedules);
	public void reveivePlace(List<Place> places);
	public void isConnected();

}
