package com.master.cityglasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class BitmapUtils {

	private static final int QUALITY = 100;
	private static final String DIR_NAME = "imageDir";

	public BitmapUtils() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Save bitmap in internal memory
	 * 
	 * @param context
	 *            application context
	 * @param bitmap
	 *            bitmap to save
	 * @param name
	 *            name of file
	 */
	public static boolean saveBitmap(Context context, Bitmap bitmap, String name) {
		boolean isSaved = false;
		ContextWrapper contextWrapper = new ContextWrapper(context);
		// path to /data/data/yourapp/app_data/imageDir
		File directory = contextWrapper.getDir(DIR_NAME, Context.MODE_PRIVATE);
		File file = new File(directory, name + ".png");
		try {
			FileOutputStream fos = new FileOutputStream(file);
			isSaved = bitmap.compress(Bitmap.CompressFormat.PNG, QUALITY, fos); // save
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return isSaved;
	}

	/**
	 * Load a bitmap in internal memory
	 * 
	 * @param context
	 *            application context
	 * @param name
	 *            name of file
	 * @return bitmap
	 */
	public static Bitmap loadBitmap(Context context, String name) {
		ContextWrapper contextWrapper = new ContextWrapper(context);
		// path to /data/data/yourapp/app_data/imageDir
		File directory = contextWrapper.getDir(DIR_NAME, Context.MODE_PRIVATE);
		File file = new File(directory, name + ".png");
		FileInputStream fis;
		Bitmap bitmap = null;
		try {
			fis = new FileInputStream(file);
			bitmap = BitmapFactory.decodeStream(fis);
		} catch (FileNotFoundException e) {
		}
		return bitmap;
	}

	/**
	 * Delete a bitmap in memory
	 * 
	 * @param context
	 *            application context
	 * @param name
	 *            name of file
	 */
	public static void deleteBitmap(Context context, String name) {
		ContextWrapper contextWrapper = new ContextWrapper(context);
		File directory = contextWrapper.getDir(DIR_NAME, Context.MODE_PRIVATE);

		showListInConsole(directory, "before delete name");

		File file = new File(directory, name + ".png");
		file.delete();

		showListInConsole(directory, "after delete name ");
	}

	/**
	 * Delete all bitmap in memory
	 * 
	 * @param context
	 */
	public static void deleteAllBitmap(Context context) {
		ContextWrapper contextWrapper = new ContextWrapper(context);
		File directory = contextWrapper.getDir(DIR_NAME, Context.MODE_PRIVATE);

		String[] names = directory.list();

		for (int i = 0; i < names.length; i++) {
			File file = new File(directory, names[i]);
			file.delete();
		}

		showListInConsole(directory, "after delete ");
	}

	public static void showFileList(Context context){
		ContextWrapper contextWrapper = new ContextWrapper(context);
		File directory = contextWrapper.getDir(DIR_NAME, Context.MODE_PRIVATE);
		showListInConsole(directory,"demande list");
	}

	private static void showListInConsole(File dir, String name) {
		String[] str = dir.list();
		for (int i = 0; i < str.length; i++) {
			Log.i("file number " + i, name + str[i]);
		}
	}

}
