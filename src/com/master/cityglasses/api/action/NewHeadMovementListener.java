package com.master.cityglasses.api.action;

public interface NewHeadMovementListener {
	
	public void updateMovement(float[] angle);
	public void movementRight();
	public void movementLeft();
	public void movementTop();
	public void movementBottom();
	public void movementCenterVertival();
	public void movementCenterHorizontal();

}
