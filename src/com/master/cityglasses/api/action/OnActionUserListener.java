package com.master.cityglasses.api.action;

public interface OnActionUserListener {
	
	public void goToRight();
	public void goToLeft();
	public void goToUp();
	public void goToBottom();
	public void goToRightWithAction();
	public void goToLeftWithAction();
	public void goToUpWithAction();
	public void goToBottomWithAction();
	public void updateGoToRightWithAction(int value);
	public void updateGoToLeftWithAction(int value);
	public void updateGoToUpWithAction(int value);
	public void updateGoToBottomWithAction(int value);

}
