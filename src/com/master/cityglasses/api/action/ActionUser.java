package com.master.cityglasses.api.action;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;

public class ActionUser {
	private static final int KEY_CODE = KeyEvent.KEYCODE_ENTER;
	private OnActionUserListener listener;
	private KeyEvent buttonEvent;
	private int keycode;

	public static final int REPEAT_DELAY = 1500;
	
	private static ActionUser myInstance = null;

	private Context context;

	private boolean isGoToLeft;
	private boolean isGoToRight;
	
	private HeadMovement movement;

	private OnHeadMovementListener headListener = new OnHeadMovementListener() {

		@Override
		public void upMovement(HeadMovementEvent headMovementEvent) {
			if (listener != null) {
				if (buttonEvent != null) {
					if (buttonEvent.getKeyCode() == KEY_CODE) {
						listener.goToUpWithAction();
					} else {
						listener.goToUp();
					}
				} else {
					listener.goToUp();
				}
			}
		}

		@Override
		public void rightMovementEvent(HeadMovementEvent headMovementEvent) {
			if (listener != null) {
				if (buttonEvent != null) {
					if (buttonEvent.getKeyCode() == KEY_CODE) {
						listener.goToRightWithAction();
					} else {
						if (!isGoToLeft) {
							if (!isGoToRight) {
								listener.goToRight();
								isGoToRight = true;
							}

						} else {
							isGoToLeft = false;
						}
					}
				} else {
					if (!isGoToLeft) {
						if (!isGoToRight) {
							listener.goToRight();
							isGoToRight = true;
						}

					} else {
						isGoToLeft = false;
					}
				}
			}

		}

		@Override
		public void leftMovementEvent(HeadMovementEvent headMovementEvent) {
			if (listener != null) {
				if (buttonEvent != null) {
					if (buttonEvent.getKeyCode() == KEY_CODE) {
						listener.goToLeftWithAction();
					} else {
						if (!isGoToRight) {
							if (!isGoToLeft) {
								listener.goToLeft();
								isGoToLeft = true;
							}

						} else {
							isGoToRight = false;
						}
					}
				} else {
					if (!isGoToRight) {
						if (!isGoToLeft) {
							listener.goToLeft();
							isGoToLeft = true;
						}

					} else {
						isGoToRight = false;
					}
				}
			}

		}

		@Override
		public void bottomMovement(HeadMovementEvent headMovementEvent) {
			if (listener != null) {
				if (buttonEvent != null) {
					if (buttonEvent.getKeyCode() == KEY_CODE) {
						listener.goToBottomWithAction();
					} else {
						listener.goToBottom();
					}
				} else {
					listener.goToBottom();
				}
			}

		}

		@Override
		public void updateUpMovement(int value) {
			if (listener != null && buttonEvent != null) {
				if (buttonEvent.getKeyCode() == KEY_CODE) {
					listener.updateGoToUpWithAction(value);
				}
			}

		}

		@Override
		public void updateBottomMovement(int value) {
			if (listener != null && buttonEvent != null) {
				if (buttonEvent.getKeyCode() == KEY_CODE) {
					listener.updateGoToBottomWithAction(value);
				}
			}

		}

		@Override
		public void updateRightMovement(int value) {
			if (listener != null && buttonEvent != null) {
				if (buttonEvent.getKeyCode() == KEY_CODE) {
					listener.updateGoToRightWithAction(value);
				}
			}

		}

		@Override
		public void updateLeftMovement(int value) {

			if (listener != null && buttonEvent != null) {
				if (buttonEvent.getKeyCode() == KEY_CODE) {
					listener.updateGoToLeftWithAction(value);
				}
			}

		}
	};
	
	public static ActionUser getInstance(Context context){
		if(myInstance == null){
			Log.i("instantiate action User","instanciate");
			myInstance = new ActionUser(context);
		}
		return myInstance;
	}

	public ActionUser(Context context) {
		this.context = context;
		movement = new HeadMovement(context);
		movement.startSensor();
		movement.setOnHeadMovementListener(headListener);
	}
	
	public void unregisterd(){
		movement.stopSensor();
	}

	public void setOnActionUserListener(OnActionUserListener listener) {
		this.listener = listener;
	}

	public OnActionUserListener getOnClickListener() {
		return listener;
	}

	public void setKeyEvent(KeyEvent keyEvent) {
		Log.i("key event","keyEvent"+keyEvent);
		this.buttonEvent = keyEvent;
	}

}
