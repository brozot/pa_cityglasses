package com.master.cityglasses.api.action;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.KeyEvent;

public class NewActionUser implements NewHeadMovementListener {
	
	private static final int KEY_CODE = KeyEvent.KEYCODE_ENTER;
	
	private List<NewActionUserListener> listeners = new ArrayList<NewActionUserListener>();
	private NewHeadMovement headMovement;
	
	private boolean isButton = false;
	
	public NewActionUser(Context context) {
		headMovement = new NewHeadMovement(context);
		headMovement.addMovementListener(this);
	}

	@Override
	public void updateMovement(float[] angle) {
		for (NewActionUserListener listener : listeners) {
			listener.updateMovement(angle, isButton);
		}
	}

	@Override
	public void movementRight() {
		for (NewActionUserListener listener : listeners) {
			listener.movementRight(isButton);
		}
	}

	@Override
	public void movementLeft() {
		for (NewActionUserListener listener : listeners) {
			listener.movementLeft(isButton);
		}
	}

	@Override
	public void movementTop() {
		for (NewActionUserListener listener : listeners) {
			listener.movementTop(isButton);
		}
	}

	@Override
	public void movementBottom() {
		for (NewActionUserListener listener : listeners) {
			listener.movementBottom(isButton);
		}
	}
	

	@Override
	public void movementCenterVertival() {
		for (NewActionUserListener listener : listeners) {
			listener.movementCenterVertical(isButton);
		}
		
	}

	@Override
	public void movementCenterHorizontal() {
		for (NewActionUserListener listener : listeners) {
			listener.movementCenterHorizontal(isButton);
		}
		
	}
	
	/**
	 * add a listener for movement
	 * @param headMovementListener listerner
	 */
	public void addActionUserListener(NewActionUserListener listener){
		this.listeners.add(listener);
	}
	
	public void setEventButton(KeyEvent event){
		isButton = false;
		if(event!=null){
			if(event.getKeyCode() == KEY_CODE){
				isButton = true;
			}
		}
	}
	
	public void resetOrigin(){
		headMovement.resetOrigin();
	}


}
