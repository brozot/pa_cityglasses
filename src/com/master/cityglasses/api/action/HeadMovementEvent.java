package com.master.cityglasses.api.action;

public class HeadMovementEvent {
	
	public static final int AXE_UP_BOTTOM = 0;
	public static final int AXE_LEFT_RIGHT = 1;
	
	private int axe;
	private long duration;
	
	public HeadMovementEvent(int axe, long duration) {
		this.axe = axe;
		this.duration = duration;
	}

	public int getAxe() {
		return axe;
	}

	public void setAxe(int axe) {
		this.axe = axe;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	
	

}
