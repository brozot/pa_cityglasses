package com.master.cityglasses.api.action;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class NewHeadMovement {

	// angle 0 haut - bas
	// angle 1 droite - gauche

	private static final float NANOSECONDE_PER_SECOND = 1.0f / 1000000000.0f;
	public static final float LIMIT_RIGHT = -0.8f;
	public static final float LIMIT_LEFT = 0.8f;
	public static final float LIMIT_TOP = 0.6f;
	public static final float LIMIT_BOTTOM = -0.6f;

	private SensorManager sensorManager;

	private Context context;

	private long lastTime = 0;
	private float[] angle = new float[3];

	private boolean isRight = false;
	private boolean isLeft = false;
	private boolean isTop = false;
	private boolean isBottom = false;

	private List<NewHeadMovementListener> headMovementListener = new ArrayList<NewHeadMovementListener>();

	private SensorEventListener listener = new SensorEventListener() {

		@Override
		public void onSensorChanged(SensorEvent event) {
			if (lastTime != 0) {
				final float dt = (event.timestamp - lastTime)
						* NANOSECONDE_PER_SECOND;
				angle[0] += event.values[0] * dt;
				angle[1] += event.values[1] * dt;
				angle[2] += event.values[2] * dt;
			}
			lastTime = event.timestamp;

			updateMovement();

		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	public NewHeadMovement(Context context) {
		this.context = context;
		this.sensorManager = (SensorManager) this.context
				.getSystemService(Context.SENSOR_SERVICE);
		startGyroscope();

	}

	public void startGyroscope() {

		Sensor gyroscope = sensorManager
				.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		// si pas de gyroscope interuption
		if (gyroscope == null) {
			return;
		}

		sensorManager.registerListener(listener, gyroscope,
				SensorManager.SENSOR_DELAY_UI);
	}

	/**
	 * add a listener for movement
	 * 
	 * @param headMovementListener
	 *            listerner
	 */
	public void addMovementListener(NewHeadMovementListener headMovementListener) {
		this.headMovementListener.add(headMovementListener);
	}

	private void updateMovement() {

		for (NewHeadMovementListener listener : headMovementListener) {
			listener.updateMovement(angle);

			if (angle[1] <= LIMIT_RIGHT && !isRight) {
				isRight = true;
				listener.movementRight();
			} else if (angle[1] > LIMIT_RIGHT) {
				if (isRight) {
					listener.movementCenterHorizontal();
				}
				isRight = false;
			}

			if (angle[1] >= LIMIT_LEFT && !isLeft) {
				isLeft = true;
				listener.movementLeft();
			} else if (angle[1] < LIMIT_LEFT) {
				if (isLeft) {
					listener.movementCenterHorizontal();
				}
				isLeft = false;
			}

			if (angle[0] <= LIMIT_BOTTOM && !isBottom) {
				isBottom = true;
				listener.movementBottom();
			} else if (angle[0] > LIMIT_BOTTOM) {
				if (isBottom) {
					listener.movementCenterVertival();
				}
				isBottom = false;
			}

			if (angle[0] >= LIMIT_TOP && !isTop) {
				isTop = true;
				listener.movementTop();
			} else if (angle[0] < LIMIT_TOP) {
				if (isTop) {
					listener.movementCenterVertival();
				}
				isTop = false;
			}

		}
	}

	public void resetOrigin() {
		Log.i("reset origin", "reset origin");
		for (int i = 0; i < 3; i++) {
			angle[i] = 0;
		}
	}

}
