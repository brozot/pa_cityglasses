package com.master.cityglasses.api.action;


public interface OnHeadMovementListener {
	
	public void upMovement(HeadMovementEvent headMovementEvent);
	public void bottomMovement(HeadMovementEvent headMovementEvent);
	public void rightMovementEvent(HeadMovementEvent headMovementEvent);
	public void leftMovementEvent(HeadMovementEvent headMovementEvent);
	public void updateUpMovement(int value);
	public void updateBottomMovement(int value);
	public void updateRightMovement(int value);
	public void updateLeftMovement(int value);

}
