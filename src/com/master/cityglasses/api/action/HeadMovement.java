package com.master.cityglasses.api.action;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.TextView;

public class HeadMovement {
	
	private OnHeadMovementListener listener;
	private static final float NANOSECONDE_PER_SECOND = 1.0f / 1000000000.0f;
	private SensorManager sensorManager;
	private boolean isSensorRegistred = false;
	
	long delta = 1000000000;
	private long epsilonTime = 1000000000;
	private float epsilonMovement =  0.01f;
	private long[] timeBeforMovement = new long[3];
	
	private float limitLeftRight = 0.4f;
	private float limitTopBottom = 0.4f;
	
	TextView view0;
	TextView view1;
	TextView view2;
	
	private long lastTime = 0;
	private float[] angle = new float[3];
	
	private Context context;
	
	private boolean[] isInitialized = {false, false, false};
	
	private float[] oldAngle = new float[3];
	long lastMovementTime[] = new long[3];
	
	float angleoldO = 0;
	long[] oldtime = new long[3];
	
	private SensorEventListener myGyroListener = new SensorEventListener() {
		
		//TODO regarder si le sensor ne devrait pas être lu et traiter dans un autre thread
		
		@Override
		public void onSensorChanged(SensorEvent event) {
			if(lastTime != 0){
				final float dt = (event.timestamp - lastTime) * NANOSECONDE_PER_SECOND;
				angle[0] += event.values[0] * dt;
				angle[1] += event.values[1] * dt;
				angle[2] += event.values[2] * dt;
				updateMovement(event);
			}else{
				for(int i=0 ; i<3 ; i++){
					lastMovementTime[i] = event.timestamp;
					timeBeforMovement[i] = event.timestamp;
				}
			}
			
			lastTime = event.timestamp;
			
		}
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {}
	};
	
	public void updateMovement(SensorEvent event){
		
		for(int i=0 ; i<3 ; i++){
			if(angle[i] > oldAngle[i]+epsilonMovement || angle[i] <oldAngle[i]-epsilonMovement){
				oldAngle[i] = angle[i];
				lastMovementTime[i] = event.timestamp;
				movementSelection(event, i, limitTopBottom, limitLeftRight);
			}else{

				//Si le temps entre le dernier déplacement et ce déplacement est plus grand que l'epsilonTime (temps de réinitialisation) nouvelle référence
				if(event.timestamp > lastMovementTime[i]+epsilonTime){
					timeBeforMovement[i] = event.timestamp;
					angle[i]=0;
					
					if(!isInitialized[i]  && listener != null){
						if(i==0){
							listener.updateUpMovement(0);
							listener.updateBottomMovement(0);
						}else if(i==1){
							listener.updateLeftMovement(0);
							listener.updateRightMovement(0);
						}
					}
					
					isInitialized[i] = true;
					
				}
				
				
				
			}
		}
		
	}
	
	public HeadMovement(Context context) {
		this.context = context;
	
		Log.i("context","context"+context);
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		startSensor();
	}
	
	private void movementSelection(SensorEvent event, int axe, float limitUpBottom, float limitLeftRight){
		//axe 0 haut bas
		HeadMovementEvent headMovementEvent = new HeadMovementEvent(axe, (event.timestamp - timeBeforMovement[axe])/100000);

		if(axe == 0 && listener != null){
			//haut
			if(angle[axe]>limitUpBottom){
				angle[axe] = 0;
				listener.upMovement(headMovementEvent);
			}else if(angle[axe]< -limitUpBottom){ //bas
				Log.i("movement","movement angle axe : "+angle[axe]);
				angle[axe] = 0;
				listener.bottomMovement(headMovementEvent);
			}else{
				if(angle[axe]>=0){
					listener.updateUpMovement(updateToPercent(angle[axe], limitUpBottom));
				}
				if(angle[axe]<=0){
					listener.updateBottomMovement(updateToPercent(angle[axe], limitUpBottom));
				}
			}
		}
		//axe 1 droite gauche
		if(axe == 1 && listener != null){
			//gauche
			if(angle[axe]>limitLeftRight && angle[axe]>0){
				angle[axe] = 0;
				listener.leftMovementEvent(headMovementEvent);
			}
			else if(angle[axe] < -limitLeftRight && angle[axe]<0){//droite
				angle[axe] = 0;
				listener.rightMovementEvent(headMovementEvent);
			}else{
				if(angle[axe] >= 0){
					listener.updateLeftMovement(updateToPercent(angle[axe], limitLeftRight));
				}
				if(angle[axe] <=0 ){
					listener.updateRightMovement(updateToPercent(angle[axe], limitLeftRight));
				}
			}
		}
		
	}
	
	public void setOnHeadMovementListener(OnHeadMovementListener listener){
		this.listener = listener;
	}
	
	public OnHeadMovementListener setOnHeadMovementListener(){
		return listener;
	}
	
	public void stopSensor(){
		if(isSensorRegistred){
			isSensorRegistred = false;
			sensorManager.unregisterListener(myGyroListener);
		}
		
	}
	
	public void startSensor(){
		if(!isSensorRegistred){
			isSensorRegistred = true;
			sensorManager.registerListener(myGyroListener, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_GAME);
		}
		
	}
	
	public boolean isSensorRunning(){
		return isSensorRegistred;
		
	}
	
	private static int updateToPercent(float value, float limit){
		int test = (int) ((value*100)/limit);
		return test;
		
	}
	
	

}
