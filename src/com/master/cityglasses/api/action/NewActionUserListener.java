package com.master.cityglasses.api.action;

public interface NewActionUserListener {
	
	public void updateMovement(float[] angle, boolean isButton);
	public void movementRight(boolean isButton);
	public void movementLeft(boolean isButton);
	public void movementTop(boolean isButton);
	public void movementBottom(boolean isButton);
	public void movementCenterHorizontal(boolean isButton);
	public void movementCenterVertical(boolean isButton);


}
