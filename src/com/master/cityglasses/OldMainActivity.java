package com.master.cityglasses;

import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.master.cityglasses.api.action.ActionUser;
import com.master.cityglasses.api.action.OnActionUserListener;
import com.master.cityglassessmartphone.domain.Request;
import com.master.cityglassessmartphone.domain.Schedule;

public class OldMainActivity extends Activity implements FragmentListener {
	
	BluetoothManager manager;
	BluetoothAdapter adapter;
	WebView web;
	
	MainFragment mainFragment;
	ScheduleFragment scheduleFragment;
	
	ImageButton button;
	
	Context context;
	ActionUser action;
	boolean test = false;
	
	List<Schedule> schedules;
	
	LinearLayout ln;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = this;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_main);
		adapter = BluetoothAdapter.getDefaultAdapter();
		Log.i("discoversable",""+adapter.getScanMode());
		startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE), 12);
		
		FragmentManager frManager = getFragmentManager();
		FragmentTransaction frTransaction = frManager.beginTransaction();
		mainFragment = MainFragment.newInstance();
		scheduleFragment = ScheduleFragment.newInstance();
		
		frTransaction.add(R.id.activity_main_container, mainFragment , "main_fragment");
		
		frTransaction.commit();
		
		
		
		//TextView view1 = (TextView) findViewById(R.id.view1);
//		TextView view2 = (TextView) findViewById(R.id.view2);
//		
		action = new ActionUser(this);
		action.setOnActionUserListener(new OnActionUserListener() {
			
			@Override
			public void goToUpWithAction() {
				Toast.makeText(context, "up action", 100).show();	
				
			}
			
			@Override
			public void goToUp() {
				Log.i("message","go to sup");
				Toast.makeText(context, "upp", Toast.LENGTH_SHORT).show();
				if(!test){
					test = false;
					String uri = "http://transport.opendata.ch/v1/locations?x=46.9900203&y=6.9290684,17";
					Request request = new Request(Request.TYPE_REQUEST_SCHEDULE, uri);
					manager.sendBluetoothObject(request);
				}
				
				
			}
			
			@Override
			public void goToRightWithAction() {
				Toast.makeText(context, "right action", 100).show();	
				
			}
			
			@Override
			public void goToRight() {
				Toast.makeText(context, "right", 100).show();
				if(scheduleFragment != null){
					if(scheduleFragment.isVisible()){
						scheduleFragment.goToRight();
					}
				}
				
			}
			
			@Override
			public void goToLeftWithAction() {
				FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
				frTransaction.setCustomAnimations(R.anim.fragment_slide_in_left, R.anim.fragment_slide_out_right);
				frTransaction.replace(R.id.activity_main_container, scheduleFragment, "scheduleFragment");
				frTransaction.commit();
				action.setKeyEvent(null);
				Toast.makeText(context, "left action", 100).show();	
				
			}
			
			@Override
			public void goToLeft() {
				Toast.makeText(context, "left", 100).show();
				if(scheduleFragment != null){
					if(scheduleFragment.isVisible()){
						scheduleFragment.goToLeft();
					}
				}	
				
			}
			
			@Override
			public void goToBottomWithAction() {
				Toast.makeText(context, "bottom action", 100).show();	
				
			}
			
			@Override
			public void goToBottom() {
				Toast.makeText(context, "bottom", 100).show();
				mainFragment.setInvisibleSchedule();
				
			}

			@Override
			public void updateGoToRightWithAction(int value) {
				Log.i("updateValue","updateValue"+value);
				if(mainFragment != null){
					mainFragment.setRightValue(value);
				}
				
			}

			@Override
			public void updateGoToLeftWithAction(int value) {
				// TODO Auto-generated method stub
				if(mainFragment != null){
					mainFragment.setLeftValue(value);
				}
				
				
			}

			@Override
			public void updateGoToUpWithAction(int value) {
				// TODO Auto-generated method stub
				if(mainFragment != null){
				}
				
				
			}

			@Override
			public void updateGoToBottomWithAction(int value) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		manager = BluetoothManager.getInstance(this);
		
		Request request = new Request(Request.TYPE_REQUEST_MAP, "https://maps.googleapis.com/maps/api/staticmap?center=46.9904597,6.9290075&&size=600x300&sensor=false&zoom=18&markers=46.9904597,6.9290075");
		//manager.sendBluetoothObject(request);
		Log.i("discoverable",""+adapter.getScanMode());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}
	
	@Override
	protected void onDestroy() {
		Log.i("manager",""+manager);
		
		super.onDestroy();
	}
	
	@Override
	protected void onPause() {
		if(manager != null){
			manager.cancel();
		}
		
		super.onPause();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.i("keyDowb","down"+event.getKeyCode());
		Log.i("keyDowb","down"+keyCode);
		action.setKeyEvent(event);
		
		if(keyCode == 4){
			finish();
		}
		
		if(keyCode == 21){
			mainFragment.incrementMapZoom();
		}
		if(keyCode == 22){
			mainFragment.decrementMapZoom();
		}
		return true;
	}

	@Override
	public void newSchedule(List<Schedule> schedules) {
		this.schedules = schedules;
		Log.i("setSchedules info","setSchedules"+schedules.size());
		if(scheduleFragment != null){
			Log.i("setSchedules info","setSchedules"+schedules.size());
			scheduleFragment.setSchedules(schedules);
		}
		
	}

}
