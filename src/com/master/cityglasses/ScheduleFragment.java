package com.master.cityglasses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.master.cityglassessmartphone.domain.Schedule;

@SuppressLint("ResourceAsColor")
public class ScheduleFragment extends Fragment{
	
	private LinearLayout leftEntryLayout;
	private LinearLayout leftLayout;
	private LinearLayout centerLayout;
	private LinearLayout rightLayout;
	private LinearLayout rightEntryLayout;
	
	private TextView leftHourTextView;
	private TextView leftNumberLigneTextView;
	private TextView leftFromTextView;
	private TextView leftToTextView;
	
	private TextView rightHourTextView;
	private TextView rightNumberLigneTextView;
	private TextView rightFromTextView;
	private TextView rightToTextView;
	
	private TextView centerHourTextView;
	private TextView centerNumberLigneTextView;
	private TextView centerFromTextView;
	private TextView centerToTextView;
	
	private TextView entryHourTextView;
	private TextView entryNumberLigneTextView;
	private TextView entryFromTextView;
	private TextView entryToTextView;
	
	private ProgressPoint progressPoint;
	private List<Schedule> schedules = new ArrayList<Schedule>();
	private int position = 0;
	
	private TextToSpeech tts;
	
	private boolean isBlocked = false;
	
	public static ScheduleFragment newInstance(){
		ScheduleFragment fragment = new ScheduleFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	public ScheduleFragment() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_schedule, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
//		leftLayout = (LinearLayout) view.findViewById(R.id.fragment_schedule_left);
//		centerLayout = (LinearLayout) view.findViewById(R.id.fragment_schedule_center);
//		rightLayout = (LinearLayout) view.findViewById(R.id.fragment_schedule_right);
//		rightEntryLayout = (LinearLayout) view.findViewById(R.id.fragment_schedule_entry);
//		
//		leftHourTextView = (TextView) view.findViewById(R.id.fragment_schedule_left_hour);
//		leftNumberLigneTextView = (TextView) view.findViewById(R.id.fragment_schedule_left_numberligne);
//		leftFromTextView = (TextView) view.findViewById(R.id.fragment_schedule_left_from);
//		leftToTextView = (TextView) view.findViewById(R.id.fragment_schedule_left_to);
//		
//		rightHourTextView = (TextView) view.findViewById(R.id.fragment_schedule_right_hour);
//		rightNumberLigneTextView = (TextView) view.findViewById(R.id.fragment_schedule_right_numberligne);
//		rightFromTextView = (TextView) view.findViewById(R.id.fragment_schedule_right_from);
//		rightToTextView = (TextView) view.findViewById(R.id.fragment_schedule_right_to);
//		
//		centerHourTextView = (TextView) view.findViewById(R.id.fragment_schedule_center_hour);
//		centerNumberLigneTextView = (TextView) view.findViewById(R.id.fragment_schedule_center_numberligne);
//		centerFromTextView = (TextView) view.findViewById(R.id.fragment_schedule_center_from);
//		centerToTextView = (TextView) view.findViewById(R.id.fragment_schedule_center_to);
//		
//		entryHourTextView = (TextView) view.findViewById(R.id.fragment_schedule_entry_hour);
//		entryNumberLigneTextView = (TextView) view.findViewById(R.id.fragment_schedule_entry_numberligne);
//		entryFromTextView = (TextView) view.findViewById(R.id.fragment_schedule_entry_from);
//		entryToTextView = (TextView) view.findViewById(R.id.fragment_schedule_entry_to);
//		
//		progressPoint = (ProgressPoint) view.findViewById(R.id.fragment_schdule_progresspoint);
		progressPoint.setChecked(R.color.red);
		progressPoint.setUncheckedColor(R.color.white);
		progressPoint.setNumber(schedules.size());
		progressPoint.setChecked(position+1);
		progressPoint.setBackgroundResource(R.color.green);
		update();
		
		
		
	}
	
	public void goToRight(){
		Log.i("go to right","posistion"+position);
		if(position < schedules.size()-1 && (!isBlocked)){
			Animation animationIn = AnimationUtils.loadAnimation(getActivity(), R.anim.view_schedule_slide_in_right);
			Animation animationOut = AnimationUtils.loadAnimation(getActivity(), R.anim.view_schedule_slide_out_right);
			
			animationIn.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					if(position+2 < schedules.size()){
						updateTextEntry(schedules.get(position+2));
					}else{
						Log.i("update","update go to right entry");
						updateTextEntry(null);
					}
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					position++;
					progressPoint.setChecked(position+1);
					update();
				}
			});
			
			//Mise en place du layout � l'�chelle de autres vignettes
			int width = rightLayout.getWidth();
			LayoutParams params = rightEntryLayout.getLayoutParams();
			params.width = width;
			rightEntryLayout.setLayoutParams(params);
			
			//lancement des animation
			rightEntryLayout.startAnimation(animationIn);
			rightLayout.startAnimation(animationOut);
			centerLayout.startAnimation(animationOut);
			leftLayout.startAnimation(animationOut);
		}
		
	}
	
	public void goToLeft(){
		Log.i("go to left","0position"+position);
		if(position>0 && (!isBlocked)){
			Animation animationIn = AnimationUtils.loadAnimation(getActivity(), R.anim.view_schedule_slide_in_left);
			Animation animationOut = AnimationUtils.loadAnimation(getActivity(), R.anim.view_schedule_slide_out_left);
			
			animationIn.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					if(position-2 < 0){
						Log.i("update","update go to left entry "+position);
						updateTextEntry(null);
					}else{
						Log.i("update","update go to left "+position);
						updateTextEntry(schedules.get(position-2));
					}
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					position --;
					progressPoint.setChecked(position+1); //position commence a 0;
					update();
					
				}
			});
			
			int width = rightLayout.getWidth();
			LayoutParams params = rightEntryLayout.getLayoutParams();
			params.width = width;
			rightEntryLayout.setLayoutParams(params);
			rightEntryLayout.startAnimation(animationIn);
			rightLayout.startAnimation(animationOut);
			centerLayout.startAnimation(animationOut);
			leftLayout.startAnimation(animationOut);
		}
		
	}
	
	public void setSchedules(List <Schedule> schedules) {
		Log.i("schedule size","det schedule"+schedules.size());
		this.schedules = null;
		
		this.schedules = new ArrayList<Schedule>(schedules);
		if(progressPoint != null){
			progressPoint.setNumber(this.schedules.size());
		}
		
		Collections.sort(this.schedules);		
	}
	
	private void update(){
		Log.i("schedule sizess","schedule size"+schedules.size());
		if(!schedules.isEmpty()){
			
				if(position == 0){
					if(position+1 < schedules.size()){
						Log.i("update","finish");
						updateText(null, schedules.get(position), schedules.get(position+1));
					}else{// si il n'y a que horaire 
						updateText(null, schedules.get(position), null);
					}
				}else if(!(position < schedules.size()-1)){ // derni�re position
					Log.i("update ici","update postion "+position);
					updateText(schedules.get(position-1), schedules.get(position), null);
				}else{
					updateText(schedules.get(position-1), schedules.get(position), schedules.get(position+1));
				}
		}
	}
	
	private void updateText(Schedule schedule1, Schedule schedule2, Schedule schedule3){
		
		if(position == 0){ // si tout a droite
			rightLayout.setVisibility(View.INVISIBLE);
		}else if(position == schedules.size()-1){//Si toute � gauche
			leftLayout.setVisibility(View.INVISIBLE);		
		}else if(schedules.size() == 1){ // si 1 seule pi�ce
			rightLayout.setVisibility(View.INVISIBLE);
			leftLayout.setVisibility(View.INVISIBLE);	
		}else{ // si tout pr�sent
			rightLayout.setVisibility(View.VISIBLE);
			leftLayout.setVisibility(View.VISIBLE);
		}
		
		rightEntryLayout.setVisibility(View.INVISIBLE);
		
		if(schedule1 != null){
			rightHourTextView.setText(schedule1.getDeparture().getHours()+":"+schedule1.getDeparture().getMinutes());
			rightNumberLigneTextView.setText(schedule1.getLigneNumber());
			rightFromTextView.setText(schedule1.getName());
			rightToTextView.setText(schedule1.getTo());
		}else{
			rightHourTextView.setText("");
			rightNumberLigneTextView.setText("");
			rightFromTextView.setText("");
			rightToTextView.setText("");
		}
		
		if(schedule2 != null){
			centerHourTextView.setText(schedule2.getDeparture().getHours()+":"+schedule2.getDeparture().getMinutes());
			centerNumberLigneTextView.setText(schedule2.getLigneNumber());
			centerFromTextView.setText(schedule2.getName());
			centerToTextView.setText(schedule2.getTo());
		}else{
			centerHourTextView.setText("");
			centerNumberLigneTextView.setText("");
			centerFromTextView.setText("");
			centerToTextView.setText("");
		}
		
		if(schedule3 != null){
			leftHourTextView.setText(schedule3.getDeparture().getHours()+":"+schedule3.getDeparture().getMinutes());
			leftNumberLigneTextView.setText(schedule3.getLigneNumber());
			leftFromTextView.setText(schedule3.getName());
			leftToTextView.setText(schedule3.getTo());
		}else{
			leftHourTextView.setText("");
			leftNumberLigneTextView.setText("");
			leftFromTextView.setText("");
			leftToTextView.setText("");
		}
		
		
	}
	
	private void updateTextEntry(Schedule schedule){
		if(schedule != null){
			rightEntryLayout.setAlpha(1.0f);
			rightEntryLayout.setVisibility(View.VISIBLE);
			entryHourTextView.setText(schedule.getDeparture().getHours()+":"+schedule.getDeparture().getMinutes());
			entryNumberLigneTextView.setText(schedule.getLigneNumber());
			entryFromTextView.setText(schedule.getName());
			entryToTextView.setText(schedule.getTo());
		}else{
			rightEntryLayout.setVisibility(View.INVISIBLE);
			rightEntryLayout.setAlpha(0);
		}
		
	}
	
	public void blockAndUnblock(){
		if(isBlocked){
			isBlocked = false;
			progressPoint.setBackgroundResource(R.color.green);
		}else{
			isBlocked = true;
			progressPoint.setBackgroundResource(R.color.yellow);
		}
	}
	
	public void setTts(TextToSpeech tts){
		
	}
	

}
