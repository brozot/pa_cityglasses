package com.master.cityglasses;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.master.cityglassessmartphone.domain.Place;

public class PlaceDetailFragment extends Fragment{
	
	private TextView nameTextView;
	private TextView distanceTextView;
	private TextView cardinalTextView;
	private TextView explainTextView;
	private ImageView imageImageView;
	
	private Place place;
	
	private Context context;
	
	/**
	 * Get a new instance of PlaceDetailFragment
	 * @return a new instance of PlaceFragment
	 */
	public static PlaceDetailFragment newInstance(){
		PlaceDetailFragment fragment = new PlaceDetailFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	public PlaceDetailFragment() {
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_place_detail, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		nameTextView = (TextView) view.findViewById(R.id.fragment_place_detail_name);
		distanceTextView = (TextView) view.findViewById(R.id.fragment_place_detail_distance);
		cardinalTextView = (TextView) view.findViewById(R.id.fragment_place_detail_cardinal);
		explainTextView = (TextView) view.findViewById(R.id.fragment_place_detail_explain);
		imageImageView = (ImageView) view.findViewById(R.id.fragment_place_detail_picture);
		
		updatePlace();
	}
	
	/**
	 * set place
	 * @param place place
	 */
	public void setPlace(Place place){
		this.place = place;
		updatePlace();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = getActivity();
	}
	
	//réinitialise les champs
	private void updatePlace(){
		if(place != null && nameTextView != null && distanceTextView != null && cardinalTextView != null && explainTextView != null && imageImageView != null){
			nameTextView.setText(place.getName());
			distanceTextView.setText(place.getId());
			cardinalTextView.setText("N");
			explainTextView.setText(place.getReference());
			
			if(context!=null){
				Bitmap bitmap = place.loadInMemory(context);
				if(bitmap!=null){
					imageImageView.setImageBitmap(bitmap);
				}
			}
			
		}
	}

}
