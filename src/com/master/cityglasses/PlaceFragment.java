package com.master.cityglasses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.master.cityglassessmartphone.domain.Place;
import com.master.cityglassessmartphone.domain.Schedule;

@SuppressLint("ResourceAsColor")
public class PlaceFragment extends Fragment{
	
	private LinearLayout leftEntryLayout;
	private LinearLayout leftLayout;
	private LinearLayout centerLayout;
	private LinearLayout rightLayout;
	private LinearLayout entryLayout;
	
	private TextView leftNameTextView;
	private TextView leftDistanceTextView;
	
	private TextView rightNameTextView;
	private TextView rightDistanceTextView;
	
	private TextView centerNameTextView;
	private TextView centerDistanceTextView;
	
	private TextView entryNameTextView;
	private TextView entryDistanceTextView;
	
	private ProgressPoint progressPoint;
	private List<Place> places = new ArrayList<Place>();
	private int position = 0;
	
	private boolean isBlocked = false;
	
	public static PlaceFragment newInstance(){
		PlaceFragment fragment = new PlaceFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	public PlaceFragment() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_place, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		leftLayout = (LinearLayout) view.findViewById(R.id.fragment_place_left);
		centerLayout = (LinearLayout) view.findViewById(R.id.fragment_place_center);
		rightLayout = (LinearLayout) view.findViewById(R.id.fragment_place_right);
		entryLayout = (LinearLayout) view.findViewById(R.id.fragment_place_entry);
		
		leftNameTextView = (TextView) view.findViewById(R.id.fragment_place_left_name);
		leftDistanceTextView = (TextView) view.findViewById(R.id.fragment_place_left_distance);
		
		rightNameTextView = (TextView) view.findViewById(R.id.fragment_place_right_name);
		rightDistanceTextView = (TextView) view.findViewById(R.id.fragment_place_right_distance);
		
		centerNameTextView = (TextView) view.findViewById(R.id.fragment_place_center_name);
		centerDistanceTextView = (TextView) view.findViewById(R.id.fragment_place_center_distance);
		
		entryNameTextView = (TextView) view.findViewById(R.id.fragment_place_entry_name);
		entryDistanceTextView = (TextView) view.findViewById(R.id.fragment_place_entry_distance);
		
		progressPoint = (ProgressPoint) view.findViewById(R.id.fragment_place_progresspoint);
		progressPoint.setChecked(R.color.red);
		progressPoint.setUncheckedColor(R.color.white);
		progressPoint.setNumber(places.size());
		progressPoint.setChecked(position+1);
		progressPoint.setBackgroundResource(R.color.green);
		update();
		
	}
	
	public void goToRight(){
		Log.i("go to right","posistion"+position);
		if(position < places.size()-1 && (!isBlocked)){
			Animation animationIn = AnimationUtils.loadAnimation(getActivity(), R.anim.view_schedule_slide_in_right);
			Animation animationOut = AnimationUtils.loadAnimation(getActivity(), R.anim.view_schedule_slide_out_right);
			
			animationIn.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					if(position+2 < places.size()){
						updateTextEntry(places.get(position+2));
					}else{
						Log.i("update","update go to right entry");
						updateTextEntry(null);
					}
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					position++;
					progressPoint.setChecked(position+1);
					update();
				}
			});
			
			//Mise en place du layout � l'�chelle de autres vignettes
			int width = rightLayout.getWidth();
			LayoutParams params = entryLayout.getLayoutParams();
			params.width = width;
			entryLayout.setLayoutParams(params);
			
			//lancement des animation
			entryLayout.startAnimation(animationIn);
			rightLayout.startAnimation(animationOut);
			centerLayout.startAnimation(animationOut);
			leftLayout.startAnimation(animationOut);
		}
		
	}
	
	public void goToLeft(){
		Log.i("go to left","0position"+position);
		if(position>0 && (!isBlocked)){
			Animation animationIn = AnimationUtils.loadAnimation(getActivity(), R.anim.view_schedule_slide_in_left);
			Animation animationOut = AnimationUtils.loadAnimation(getActivity(), R.anim.view_schedule_slide_out_left);
			
			animationIn.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					if(position-2 < 0){
						Log.i("update","update go to left entry "+position);
						updateTextEntry(null);
					}else{
						Log.i("update","update go to left "+position);
						updateTextEntry(places.get(position-2));
					}
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					position --;
					progressPoint.setChecked(position+1); //position commence a 0;
					
				}
			});
			
			int width = rightLayout.getWidth();
			LayoutParams params = entryLayout.getLayoutParams();
			params.width = width;
			entryLayout.setLayoutParams(params);
			entryLayout.startAnimation(animationIn);
			rightLayout.startAnimation(animationOut);
			centerLayout.startAnimation(animationOut);
			leftLayout.startAnimation(animationOut);
		}
		
	}
	
	public void setPlaces(List <Place> places) {
		Log.i("schedule size","det schedule"+places.size());
		this.places = null;
		
		this.places = new ArrayList<Place>(places);
		if(progressPoint != null){
			progressPoint.setNumber(this.places.size());
		}
		
		Collections.sort(this.places);		
	}
	
	private void update(){
		Log.i("schedule sizess","schedule size"+places.size());
		if(!places.isEmpty()){
			
				if(position == 0){
					if(position+1 < places.size()){
						Log.i("update","finish");
						updateText(null, places.get(position), places.get(position+1));
					}else{// si il n'y a que horaire 
						updateText(null, places.get(position), null);
					}
				}else if(!(position < places.size()-1)){ // derni�re position
					Log.i("update ici","update postion "+position);
					updateText(places.get(position-1), places.get(position), null);
				}else{
					updateText(places.get(position-1), places.get(position), places.get(position+1));
				}
				
				if(position-2 >= 0){
					places.get(position-2).saveInMemory(getActivity());
				}
				
				if(position+2<places.size()){
					places.get(position+2).saveInMemory(getActivity());
				}
		}
	}
	
	private void updateText(Place place1, Place place2, Place place3){
		
		if(position == 0){ // si tout a droite
			rightLayout.setVisibility(View.INVISIBLE);
		}else if(position == places.size()-1){//Si toute � gauche
			leftLayout.setVisibility(View.INVISIBLE);		
		}else if(places.size() == 1){ // si 1 seule pi�ce
			rightLayout.setVisibility(View.INVISIBLE);
			leftLayout.setVisibility(View.INVISIBLE);	
		}else{ // si tout pr�sent
			rightLayout.setVisibility(View.VISIBLE);
			leftLayout.setVisibility(View.VISIBLE);
		}
		
		entryLayout.setVisibility(View.INVISIBLE);
		
		if(place1 != null){
			BitmapDrawable drawable = new BitmapDrawable(place1.loadInMemory(getActivity()));
			rightLayout.setBackgroundDrawable(drawable);
			rightNameTextView.setText(place1.getName());
			rightDistanceTextView.setText(place1.getId());
		}else{
			rightLayout.setBackgroundDrawable(null);
			rightNameTextView.setText("");
			rightDistanceTextView.setText("");
		}
		
		if(place2 != null){
			BitmapDrawable drawable = new BitmapDrawable(place2.loadInMemory(getActivity()));
			centerLayout.setBackgroundDrawable(drawable);
			centerNameTextView.setText(place2.getName());
			centerDistanceTextView.setText(place2.getId());
		}else{
			centerLayout.setBackgroundDrawable(null);
			centerNameTextView.setText("");
			centerDistanceTextView.setText("");
		}
		
		if(place3 != null){
			BitmapDrawable drawable = new BitmapDrawable(place3.loadInMemory(getActivity()));
			leftLayout.setBackgroundDrawable(drawable);
			leftNameTextView.setText(place3.getName());
			leftDistanceTextView.setText(place3.getId());
		}else{
			leftLayout.setBackgroundDrawable(null);
			leftNameTextView.setText("");
			leftDistanceTextView.setText("");
		}
		
		
	}
	
	private void updateTextEntry(Place place){
		if(place != null){
			entryLayout.setAlpha(1.0f);
			entryLayout.setVisibility(View.VISIBLE);
			BitmapDrawable drawable = new BitmapDrawable(place.loadInMemory(getActivity()));
			entryLayout.setBackgroundDrawable(drawable);
			entryNameTextView.setText(place.getName());
			entryDistanceTextView.setText(place.getId());
		}else{
			entryLayout.setBackgroundDrawable(null);
			entryLayout.setVisibility(View.INVISIBLE);
			entryLayout.setAlpha(0);
		}
		
	}
	
	public void blockAndUnblock(){
		if(isBlocked){
			isBlocked = false;
			progressPoint.setBackgroundResource(R.color.green);
		}else{
			isBlocked = true;
			progressPoint.setBackgroundResource(R.color.yellow);
		}
	}
	

}
