package com.master.cityglasses;

import java.util.List;

import com.master.cityglassessmartphone.domain.Schedule;

public interface FragmentListener {
	
	public void newSchedule(List<Schedule> schedules);
}
