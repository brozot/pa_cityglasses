package com.master.cityglasses;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class CaptureFragment extends Fragment implements Callback {

	private ImageView iv_image;
	// a variable to store a reference to the Surface View at the main.xml file
	private SurfaceView surfaceView;

	// a bitmap to display the captured image
	private Bitmap bmp;

	// Camera variables
	// a surface holder
	private SurfaceHolder surfaceHolder;
	// a variable to control the camera
	private Camera camera;
	// the camera parameters
	private Parameters parameters;

	/**
	 * Get a new instance of CaptureFragment
	 * 
	 * @return a new instance of PlaceFragment
	 */
	public static CaptureFragment newInstance() {
		CaptureFragment fragment = new CaptureFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	public CaptureFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_capture, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		surfaceView = (SurfaceView) view
				.findViewById(R.id.fragment_capture_surfaceview);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		// add the callback interface methods defined below as the Surface View
		// callbacks
		surfaceHolder.addCallback(this);

		// tells Android that this surface will have its data constantly
		// replaced
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

	}

	// Callback
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, acquire the camera and tell it where  
        // to draw the preview.  
        camera = Camera.open();  
        try {  
           camera.setPreviewDisplay(holder);  
  
        } catch (IOException exception) {  
            camera.release();  
            camera = null;  
        }  

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		//get camera parameters  
        parameters = camera.getParameters();  
 
        //set camera parameters  
        camera.setParameters(parameters);  
        camera.startPreview();  
 
        //sets what code should be executed after the picture is taken  
        
        // 
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		//stop the preview  
        camera.stopPreview();  
        //release the camera  
        camera.release();  
        //unbind the camera from this object  
        camera = null;  

	}
	
	public void takeAPicture(){
        
        camera.autoFocus(new AutoFocusCallback() {
        	
        	Camera.PictureCallback mCall = new Camera.PictureCallback()  
            {  
                @Override  
                public void onPictureTaken(byte[] data, Camera camera)  
                {  
//                    //decode the data obtained by the camera into a Bitmap  
//                    bmp = BitmapFactory.decodeByteArray(data, 0, data.length);  
//                    //set the iv_image  
//                    iv_image.setImageBitmap(bmp);
                	//FileOutputStream outStream = nul;
                	InputStream is = new ByteArrayInputStream(data);
                	Bitmap bmp = BitmapFactory.decodeStream(is);
                	MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bmp, ""+System.currentTimeMillis() , ""+System.currentTimeMillis());
                	Toast.makeText(getActivity(), "photo enregistrée", Toast.LENGTH_SHORT).show();
//                	ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                	_bitmapScaled.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
//                    try {
//                        outStream = new FileOutputStream(String.format(
//                                "/sdcard/%d.jpg", System.currentTimeMillis()));
//                        outStream.write(data);
//                        Bitmap bitmap = BitmapFactory.
//                        outStream.close();
//                        MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), yourBitmap, yourTitle , yourDescription);
//                        Log.d("Log", "onPictureTaken - wrote bytes: " + data.length);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } finally {
//                    }
                    Log.d("Log", "onPictureTaken - jpeg");
                }  
            };  
			
			@Override
			public void onAutoFocus(boolean success, Camera camera) {
				if(success){
					camera.takePicture(null, null, mCall);
					camera.startPreview();
				}
				
			}
		});
		
	}

}
