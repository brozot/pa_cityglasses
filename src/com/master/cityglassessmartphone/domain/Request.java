package com.master.cityglassessmartphone.domain;

import java.io.Serializable;

public class Request implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public static final int TYPE_REQUEST_STATION = 0;
	public static final int TYPE_REQUEST_SCHEDULE = 1;
	public static final int TYPE_REQUEST_MONUMENT = 2;
	public static final int TYPE_REQUEST_MAP = 3;
	public static final int TYPE_REQUEST_NAVIGATION = 4;
	
	private int type;
	private String uri;
	
	public Request(int type , String uri){
		this.type = type;
		this.uri = uri;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String toString() {
		return "Request [type=" + type + ", uri=" + uri + "]";
	}
	
	
	
	
}
