package com.master.cityglassessmartphone.domain;

import java.io.Serializable;
import java.util.Date;

public class Schedule extends Station implements Serializable, Comparable<Schedule>{
	
	private static final long serialVersionUID = 1L;
	
	protected String ligneNumber;
	protected String to;
	protected Date departure;
	
	public Schedule() {
		this(null, null, NULL_VALUE_DOUBLE, NULL_VALUE_DOUBLE);
	}

	public Schedule(String id, String name, double longitude, double latitude) {
		this(id, name, longitude, latitude, null , "", null);
	}
	
	public Schedule(String id, String name, double longitude, double latitude, String ligneNumber, String to, Date departure) {
		super(id, name, longitude, latitude);
		this.ligneNumber = ligneNumber;
		this.departure = departure;
		this.to = to;
	}

	public String getLigneNumber() {
		return ligneNumber;
	}

	public void setLigneNumber(String ligneNumber) {
		this.ligneNumber = ligneNumber;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	public Date getDeparture() {
		return departure;
	}
	
	public void setDeparture(Date departure) {
		this.departure = departure;
	}
	
	public void setStation(Station station){
		this.id = station.getId();
		this.name = station.getName();
		this.longitude = station.getLongitude();
		this.latitude = station.getLatitude();
	}

	@Override
	public String toString() {
		return "Schedule [ligneNumber=" + ligneNumber + ", to=" + to
				+ ", departure=" + departure + ", id=" + id + ", name=" + name
				+ ", longitude=" + longitude + ", latitude=" + latitude + "]";
	}

	@Override
	public int compareTo(Schedule another) {
		int result;
		if(this.departure.before(another.getDeparture())){
			result = 1;
		}else{
			result = -1;
		}
		
		return result;
	}
	
	
	
	
	
	
	
	
	
	

}
