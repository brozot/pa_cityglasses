package com.master.cityglassessmartphone.domain;

import java.io.Serializable;

public class Station implements Serializable {

	private static final long serialVersionUID = 1L;
	protected static final double NULL_VALUE_DOUBLE = -9999;
	protected static final int NULL_VALUE_INT = -9999;

	protected String id;
	protected String name;
	protected double longitude;
	protected double latitude;

	public Station() {
		this(null, null, NULL_VALUE_DOUBLE, NULL_VALUE_DOUBLE);
	}

	public Station(String id) {
		this(id, null, NULL_VALUE_DOUBLE, NULL_VALUE_DOUBLE);
	}

	public Station(String id, String name) {
		this(id, name, NULL_VALUE_DOUBLE, NULL_VALUE_DOUBLE);
	}
	
	public Station(Station station){
		this.id = new String(station.getId());
		this.name = new String(station.getName());
		this.longitude = station.getLongitude();
		this.latitude = station.getLatitude();
	}

	public Station(String id, String name, double longitude, double latitude) {
		this.id = id;
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	@Override
	public String toString() {
		return "Station [id=" + id + ", name=" + name + ", longitude="
				+ longitude + ", latitude=" + latitude + "]";
	}
	
	
	

}
